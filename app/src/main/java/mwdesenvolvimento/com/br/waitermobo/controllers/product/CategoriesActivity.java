package mwdesenvolvimento.com.br.waitermobo.controllers.product;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling.FastSellingActivity;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Listener;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.Category;

/**
 * Created by mindware on 27/03/15.
 */
public class CategoriesActivity extends Activity implements Listener{
  private static final String TAG = CategoriesActivity.class.getSimpleName();
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_select_category);
    TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
    txtTitle.setText(R.string.categories);
    initList();
    ListView lstView = (ListView) findViewById(R.id.lstItems);
    lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Category category = (Category) parent.getAdapter().getItem(position);
        Intent i = new Intent(CategoriesActivity.this, ProdsActivity.class);
        i.putExtra("hasFilter", true);
        i.putExtra("idCat", category.get_id());
        CategoriesActivity.this.startActivity(i);
      }
    });
    Button btnProds = (Button) findViewById(R.id.btnCheckProds);
    btnProds.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent(CategoriesActivity.this, ProdsActivity.class);
        i.putExtra("hasFilter", false);
        CategoriesActivity.this.startActivity(i);
      }
    });
    Sys.getInstance().listenToSubject(FastSellingActivity.UPDATE_ITEM_SUBJECT, this);
  }

  private void initList() {
    try {
      Dao<Category, Integer> dao = Database.getDAO(Category.class);

      List<Category> cats = dao.queryForAll();
      JsonHelper.sortList(cats, Category.class);
      JsonHelper.populateList(this, cats, R.id.lstItems,
              R.layout.ac_categories_item, CategoryAdapter.class);

    } catch(SQLException e) {
      Log.e(TAG, "Exception", e);
    }
  }

  @Override
  public void update(Map<String, Object> data) {
    String command = (String) data.get("category");
    if("close".equals(command)) finish();
  }
}
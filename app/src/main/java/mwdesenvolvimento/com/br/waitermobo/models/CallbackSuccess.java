package mwdesenvolvimento.com.br.waitermobo.models;

/**
 * Created by geppetto on 06/08/15.
 */
public interface CallbackSuccess {
  CallbackSuccess NULL = new CallbackSuccess() {
    @Override
    public void run() {}
  };
  void run();
}

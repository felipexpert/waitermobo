package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by geppetto on 26/06/15.
 */
@DatabaseTable(tableName="tbl_parameter")
public class Parameter {
  @DatabaseField(generatedId = true)
  private int _id;
  @DatabaseField(foreign = true, foreignAutoCreate = true,
      foreignAutoRefresh=true, maxForeignAutoRefreshLevel=1)
  private OrderCommand orderCommand;
  @DatabaseField
  private String parameter;
  @DatabaseField
  private String value;

  public Parameter() {}

  public Parameter(int _id, OrderCommand orderCommand, String parameter, String value) {
    this._id = _id;
    this.orderCommand = orderCommand;
    this.parameter = parameter;
    this.value = value;
  }

  public int get_id() {
    return _id;
  }

  public void set_id(int _id) {
    this._id = _id;
  }

  public OrderCommand getOrderCommand() {
    return orderCommand;
  }

  public void setOrderCommand(OrderCommand orderCommand) {
    this.orderCommand = orderCommand;
  }

  public String getParameter() {
    return parameter;
  }

  public void setParameter(String parameter) {
    this.parameter = parameter;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}

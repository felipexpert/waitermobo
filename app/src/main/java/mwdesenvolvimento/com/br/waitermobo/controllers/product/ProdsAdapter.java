package mwdesenvolvimento.com.br.waitermobo.controllers.product;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.Product;

/**
 * Created by mindware on 25/03/15.
 */
public class ProdsAdapter extends ArrayAdapter<Product> implements Filterable{
  private static final String TAG = ProdsAdapter.class.getSimpleName();
  private Context context;
  private List<Product> all;
  private List<Product> products;
  public ProdsAdapter(Context context, int resource, List<Product> products) {
    super(context, resource, products);
    this.context = context;
    this.all = this.products = products;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // 1. Create inflater
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // 2. Get rowView from inflater
    View rowView = inflater.inflate(R.layout.ac_prods_item, parent, false);

    // 3. Get the two text view from the rowView
    TextView lblCode = (TextView) rowView.findViewById(R.id.lblCode);
    //TextView lblName = (TextView) rowView.findViewById(R.id.lblName);
    TextView lblCategory = (TextView) rowView.findViewById(R.id.lblCategory);
    TextView lblPrice = (TextView) rowView.findViewById(R.id.lblPrice);

    // 4. Set the text for textView
    Product p = products.get(position);
    lblCode.setText(p.getCode() + " - " + p.getName());
    //lblName.setText(p.getName());
    lblCategory.setText(R.string.woCat);
    String category = p.getCategoryName();
    if(category != null) {
      category = category.substring(0, 1).toUpperCase() + category.substring(1);
      lblCategory.setText(category != null ? category : context.getText(R.string.woCat));
    }
    lblPrice.setText(Sys.getInstance().currencyFormat(p.getPrice()));

    // 5. return rowView
    return rowView;
  }

  @Override
  public int getCount() {
    return products.size();
  }

  @Override
  public Product getItem(int position) {
    return products.get(position);
  }

  @Override
  public Filter getFilter() {
    Filter filter = new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        List<Product> filtered = new ArrayList<>();
        String constr = constraint.toString().toUpperCase();
        for(Product p : all) {
          String name = p.getName().toUpperCase();
          if (name.startsWith(constr) || name.endsWith(constr))
            filtered.add(p);
        }
        results.values = filtered;
        results.count = filtered.size();
        Log.e(TAG, results.values.toString());
        return results;
      }

      @Override
      protected void publishResults(CharSequence constraint, FilterResults results) {
        products = (List<Product>) results.values;
        notifyDataSetChanged();
      }
    };
    return filter;
  }
}
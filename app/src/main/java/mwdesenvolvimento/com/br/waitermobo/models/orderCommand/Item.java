package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import br.com.mwdesenvolvimento.mylibrary.Util;
import mwdesenvolvimento.com.br.waitermobo.models.Product;

/**
 * Created by geppetto on 26/06/15.
 */
public class Item implements Comparable<Item> {
  private long orderItemId;
  private Product product;
  private double price;
  private double amount;
  private int displayOrder;
  private boolean onServer;

  public Item(Product product, double price, double amount, int displayOrder, boolean onServer) {
    this(-1, product, price, amount, displayOrder, onServer);
  }

  public Item(long orderItemId, Product product, double price, double amount, int displayOrder, boolean onServer) {
    this.orderItemId = orderItemId;
    this.product = product;
    this.price = price;
    this.amount = amount;
    this.displayOrder = displayOrder;
    this.onServer = onServer;
  }

  public long getOrderItemId() {
    return orderItemId;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public int getDisplayOrder() {
    return displayOrder;
  }

  public void setDisplayOrder(int displayOrder) {
    this.displayOrder = displayOrder;
  }

  public boolean isOnServer() {
    return onServer;
  }

  public void setOnServer(boolean onServer) {
    this.onServer = onServer;
  }

  @Override
  public int compareTo(Item another) { return displayOrder - another.displayOrder; }

  @Override
  public String toString() {
    return "Item {orderItemId(" + orderItemId + "),product(" + product.getName() + "),price("
            + price + "),amount(" + amount + "),displayOrder(" + displayOrder + "),onServer("
            + onServer + ")}";
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(!(o instanceof  Item)) return false;
    Item i = (Item) o;
    return orderItemId == i.orderItemId && product.get_id() == product.get_id()
        && Util.eq(price, i.price) && Util.eq(amount, i.amount)
        && displayOrder == i.displayOrder;
  }
}
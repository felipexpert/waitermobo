package mwdesenvolvimento.com.br.waitermobo.miscellaneous;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;

import br.com.mwdesenvolvimento.mylibrary.database.DatabaseHelperBehavior;
import mwdesenvolvimento.com.br.waitermobo.models.Category;
import mwdesenvolvimento.com.br.waitermobo.models.Msn;
import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.OrderItem;
import mwdesenvolvimento.com.br.waitermobo.models.Product;
import mwdesenvolvimento.com.br.waitermobo.models.Waiter;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommand;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.Parameter;

/**
 * Created by mindware on 09/03/15.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper implements DatabaseHelperBehavior {
  public static final String TAG = DatabaseHelper.class.getSimpleName();
  private static final String DATABASE_NAME = "db_waiter";
  private static final int DATABASE_VERSION = 128;

  private static final String SYSTEMS_TABLE =
      " create table " + Database.SYSTEMS_TABLE_NAME +
          " (hostname text null," +
          " id_waiter integer null," +
          " name_waiter text null," +
          " total_order_numbers integer not null);  ";

  public void resetAllEntities() {
    ConnectionSource cs = getConnectionSource();
    try {
      dropAllEntities(cs);
      createAllEntities(cs);
    } catch(SQLException e) {
      e.printStackTrace();
    }
  }

  private void createAllEntities(ConnectionSource cs) throws SQLException {
    TableUtils.createTable(cs, Msn.class);
    TableUtils.createTable(cs, Waiter.class);
    TableUtils.createTable(cs, Category.class);
    TableUtils.createTable(cs, Product.class);
    TableUtils.createTable(cs, Order.class);
    TableUtils.createTable(cs, OrderItem.class);
    TableUtils.createTable(cs, OrderCommandHeader.class);
    TableUtils.createTable(cs, OrderCommand.class);
    TableUtils.createTable(cs, Parameter.class);
  }

  private void dropAllEntities(ConnectionSource cs) throws SQLException {
    TableUtils.dropTable(cs, Parameter.class, true);
    TableUtils.dropTable(cs, OrderCommand.class, true);
    TableUtils.dropTable(cs, OrderCommandHeader.class, true);
    TableUtils.dropTable(cs, OrderItem.class, true);
    TableUtils.dropTable(cs, Order.class, true);
    TableUtils.dropTable(cs, Product.class, true);
    TableUtils.dropTable(cs, Category.class, true);
    TableUtils.dropTable(cs, Waiter.class, true);
    TableUtils.dropTable(cs, Msn.class, true);
  }

  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db, ConnectionSource cs) {
    db.execSQL(SYSTEMS_TABLE);
    db.execSQL("insert into " + Database.SYSTEMS_TABLE_NAME +
        " (hostname, id_waiter, name_waiter, total_order_numbers) values(null, null, null, 26); ");
    try {
      createAllEntities(cs);
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, ConnectionSource cs, int oldVersion, int newVersion) {
    db.execSQL("drop table if exists "  + Database.SYSTEMS_TABLE_NAME);
    try {
      dropAllEntities(cs);
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    Log.d(TAG, "tables has been dropped!");
    onCreate(db, cs);
  }

  @Override
  public ArrayList<Cursor> getData(String Query){
    //get writable database
    SQLiteDatabase sqlDB = this.getWritableDatabase();
    String[] columns = new String[] { "mesage" };
    //an array list of cursor to save two cursors one has results from the query
    //other cursor stores error message if any errors are triggered
    ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
    MatrixCursor Cursor2= new MatrixCursor(columns);
    alc.add(null);
    alc.add(null);


    try{
      String maxQuery = Query ;
      //execute the query results will be save in Cursor c
      Cursor c = sqlDB.rawQuery(maxQuery, null);


      //add value to cursor2
      Cursor2.addRow(new Object[] { "Success" });

      alc.set(1,Cursor2);
      if (null != c && c.getCount() > 0) {


        alc.set(0,c);
        c.moveToFirst();

        return alc ;
      }
      return alc;
    } catch(Exception ex){

      Log.d("printing exception", ex.getMessage());

      //if any exceptions are triggered save the error message to cursor an return the arraylist
      Cursor2.addRow(new Object[] { ""+ex.getMessage() });
      alc.set(1,Cursor2);
      return alc;
    }


  }

  /*public static final String TAG = DatabaseHelper.class.getSimpleName();
  private static final String DATABASE_NAME = "db_waiter";
  private static final int DATABASE_VERSION = 7;

  private static final String SYSTEMS_TABLE =
      " create table " + Database.SYSTEMS_TABLE_NAME +
          " (id_waiter integer null," +
          " name_waiter text null);  ";

  DatabaseHelper(Context context){
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(SYSTEMS_TABLE);
    db.execSQL("insert into " + Database.SYSTEMS_TABLE_NAME +
        " (id_waiter, name_waiter) values(null, null); ");
    Log.d(TAG, "tables has been created!");
  }
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("drop table if exists "  + Database.SYSTEMS_TABLE_NAME);
    Log.d(TAG, "tables has been dropped!");
    onCreate(db);
  }*/
}
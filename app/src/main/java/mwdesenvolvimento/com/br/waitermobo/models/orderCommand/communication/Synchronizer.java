package mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication;

import android.util.Log;

import com.j256.ormlite.dao.Dao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import br.com.mwdesenvolvimento.mylibrary.server.JsonServerException;
import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.SystemURL;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.OrderItem;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.Item;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommand;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.Parameter;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrder;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrderUtils;

/**
 * Created by geppetto on 05/08/15.
 */
public class Synchronizer {
  private static final String TAG = Synchronizer.class.getSimpleName();
  private List<OrderCommandHeader> headers;
  private CallbackSuccess success;
  private CallbackFail<SynchronizerListener> fail;
  private int successes;
  private boolean failed;
  private boolean print;

  public Synchronizer(OrderCommandHeader header, CallbackSuccess success, CallbackFail fail, boolean print) {
    this(Arrays.asList(header), success, fail, print);
  }

  public Synchronizer(List<OrderCommandHeader> headers, CallbackSuccess success, CallbackFail fail, boolean print) {
    this.headers = headers;
    this.success = success;
    this.fail = fail;
    this.print = print;
  }

  public void synchronize() {
    for(final OrderCommandHeader header : headers) {
      if(header.getOrder() != null) {
        PostRequestTask rt = new PostRequestTask(new SynchronizerListener(header), Task.FETCH_ORDER);
        rt.execute(SystemURL.GET_ORDERS.getUrl(header.getOrder().get_id()));
      } else if(VirtualOrderUtils.virtualize(header).isChanged()) {
        SynchronizerListener listener = new SynchronizerListener(header);
        try {
          updateServer(listener);
        } catch (JSONException e) {
          fail(listener);
        }
      }
    }
  }

  public  class SynchronizerListener implements TaskListener {
    private OrderCommandHeader header;
    public SynchronizerListener(OrderCommandHeader header) {
      this.header = header;
    }
    @Override
    public void act(String result, Task task, Exception e) {
      if(e == null) {
        try {
          switch (task) {
            case FETCH_ORDER: {
              List<Order> orders = JsonHelper.getRows(result, Order.class, false);
              Order orderServer = orders.get(0);
              VirtualOrder voServer = VirtualOrderUtils.virtualOrder(orderServer);
              if (!VirtualOrderUtils.virtualOrder(header.getOrder()).equals(voServer)) {
                // Server overrides local
                changeState(header, orderServer);
                if(print) print(this, orderServer.get_id());
                else success();
              } else if(VirtualOrderUtils.virtualize(header).isChanged()) {
                // try to update server
                updateServer(this);
              } else {
                // Everything is right
                success();
              }
            } break;
            case UPDATE_ORDER: {
              List<Order> orders = JsonHelper.getRows(result, Order.class, false);
              Order orderServer = orders.get(0);
              changeState(header, orderServer);
              if(print) print(this, orderServer.get_id());
              else success();
            } break;
            case PRINT: {
              success();
            } break;
            default:
              Sys.assert_(false, "Unknown task!");
          }
        } catch (SQLException | JSONException | JsonServerException | NullPointerException e2) {
          Log.e(TAG, "Exception", e2);
          fail(this);
        }
      } else {
        Log.e(TAG, "Exception", e);
        fail(this);
      }
    }

    public OrderCommandHeader getHeader() {
      return header;
    }

    public boolean isPrint() {
      return Synchronizer.this.print;
    }
  }

  private static void updateServer(SynchronizerListener listener) throws JSONException {
    VirtualOrder vo = VirtualOrderUtils.virtualize(listener.getHeader());
    PostRequestTask rt = new PostRequestTask(listener, Task.UPDATE_ORDER);
    Order order = listener.getHeader().getOrder();
    long orderId = order != null ? order.get_id() : -1;
    Log.d(TAG, toJson(vo).toString());
    rt.execute(SystemURL.SYNC_ORDER.getUrl(orderId), toJson(vo).toString());
  }

  private static void print(TaskListener listener, long orderId) {
    PostRequestTask rt = new PostRequestTask(listener, Task.PRINT);
    rt.execute(SystemURL.PRINT.getUrl(orderId));
  }

  private static JSONObject toJson(VirtualOrder virtualOrder) throws JSONException {
    JSONArray items = new JSONArray();
    for (Item item : virtualOrder.getItems()) {
      JSONObject i = new JSONObject()
          .put("orderItemId", item.getOrderItemId())
          .put("productCode", item.getProduct().getCode())
          .put("price", item.getPrice())
          .put("amount", item.getAmount())
          .put("displayOrder", item.getDisplayOrder());
      items.put(i);
    }
    return new JSONObject()
        .put("orderNumber", virtualOrder.getOrderNumber())
        .put("orderId", virtualOrder.getOrderId())
        .put("identifier", virtualOrder.getIdentifier())
        .put("waiterId", virtualOrder.getWaiter().get_id())
        .put("obs", virtualOrder.getObs())
        .put("items", items);
  }

  private static void changeState(OrderCommandHeader header, Order orderServer) throws SQLException {
    Database.getDAO(Order.class).createOrUpdate(orderServer);
    Dao<OrderItem, Integer> oiDAO = Database.getDAO(OrderItem.class);
    for(OrderItem oi : orderServer.getOrderItemCollection())
      oiDAO.createOrUpdate(oi);
    Dao<Parameter, Integer> paramDAO = Database.getDAO(Parameter.class);
    for(OrderCommand c : header.getCommands())
      paramDAO.delete(c.getParameters());
    Database.getDAO(OrderCommand.class).delete(header.getCommands());
    Database.getDAO(OrderCommandHeader.class).delete(header);
  }

  private void success() {
    if(++successes == headers.size()) {
      Log.d(TAG, "Running success " + successes + " " + headers.size());
      this.success.run();
    }
    Log.d(TAG, "succeed " + successes);
  }

  private void fail(SynchronizerListener listener) {
    if(!failed) {
      Log.d(TAG, "Running fail");
      failed = true;
      fail.run(listener);
    }
    Log.d(TAG, "failed");
  }
}

package mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication;

import android.util.Log;

import com.j256.ormlite.dao.Dao;

import org.json.JSONException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import br.com.mwdesenvolvimento.mylibrary.server.JsonServerException;
import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.SystemURL;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.OrderItem;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrderUtils;

/**
 * Created by geppetto on 05/08/15.
 */
public class FastSellingService {
  private static final String TAG = FastSellingService.class.getSimpleName();
  private static final FastSellingService INSTANCE = new FastSellingService();
  private List<Work> works = new ArrayList<>();
  private boolean initialized = false;
  private boolean alive = true;
  private boolean wait = false;

  private FastSellingService() {}

  public static void init() {
    if (INSTANCE.initialized) return;
    INSTANCE.initialized = true;
    new Thread(new Runnable() {
      @Override
      public void run() {
        while (INSTANCE.alive) {
          int tasks = 0;
          synchronized (INSTANCE) {
            if(!INSTANCE.wait) {
              // Remove duplicates and ensure print is going to
              // be set properly
              for(int i = INSTANCE.works.size() - 1; i >= 0; i--) {
                Work w1 = INSTANCE.works.get(i);
                for (int j = 0; j < INSTANCE.works.size(); j++) {
                  Work w2 = INSTANCE.works.get(j);
                  if (w1 != w2) {
                    if (w1.header.get_id() == w2.header.get_id()) {
                      if (w1.print) w2.print = true;
                      INSTANCE.works.remove(i);
                      break;
                    }
                  }
                }
              }
              // do the work and remove from stack
              for (int i = INSTANCE.works.size() - 1; i >= 0; i--) {
                Work w = INSTANCE.works.remove(i);
                INSTANCE.addTask(w.header, w.print, FastSellingServicePriority.HIGH);
                tasks++;
              }
              Sys.log("Performed works:", tasks);
            }
          }
          try { Thread.sleep(5000); } catch (InterruptedException e) {}
        }
      }
    }).start();
  }

  public static FastSellingService getInstance() {
    return INSTANCE;
  }

  public static void end() {
    INSTANCE.alive = false;
  }

  public static void setWait(boolean wait) {
    synchronized (INSTANCE) {
      INSTANCE.wait = wait;
    }
  }

  public void addTask(OrderCommandHeader header, boolean print, FastSellingServicePriority priority) {
    addTask(header, print, priority, CallbackSuccess.NULL);
  }

  public void addTask(OrderCommandHeader header, boolean print, FastSellingServicePriority priority, CallbackSuccess callback) {
    switch (priority) {
      case HIGH:
        synchronize(header, print, callback);
        break;
      case NORMAL:
        synchronized (INSTANCE) {
          works.add(new Work(header, print));
        }
        break;
      default:
        Sys.assert_(false, "Unknown priority:" + priority);
    }
  }

  public void verifyOrderNumber(final int orderNumber, final CallbackSuccess success, final CallbackFail fail) {
    PostRequestTask rt = new PostRequestTask(new TaskListener() {
      @Override
      public void act(String result, Task task, Exception e) {
        try {
          List<Order> orders = JsonHelper.getRows(result, Order.class, false);
          Dao<OrderCommandHeader, Integer> headerDao = Database.getDAO(OrderCommandHeader.class);
          Dao<Order, Long> orderDAO = Database.getDAO(Order.class, Long.class);
          Dao<OrderItem, Integer> oiDAO = Database.getDAO(OrderItem.class);
          //remove unavailable
          List<Order> localOrders = orderDAO.queryForEq("orderNumber", orderNumber);
          A:
          for(Order o : localOrders) {
            for(Order so : orders) if(so.get_id() == o.get_id()) continue A;
            List<OrderCommandHeader> headers = headerDao.queryForEq("order_id", o.get_id());
            headerDao.delete(headers);
            oiDAO.delete(o.getOrderItemCollection());
            orderDAO.delete(o);
          }
          if (orders.size() == 0) {
            success.run();
          } else {
            List<OrderCommandHeader> headers = new ArrayList<>();
            for (Order order : orders) {
              if(!orderDAO.idExists(order.get_id())) {
                for(OrderItem oi : order.getOrderItemCollection()) {
                    oiDAO.create(oi);
                }
                orderDAO.create(order);
              } else {
                order = orderDAO.queryForId(order.get_id());
              }
              List<OrderCommandHeader> hds =
                  Database.getDAO(OrderCommandHeader.class).queryForEq("order_id", order.get_id());
              if (hds.size() == 0) headers.add(VirtualOrderUtils.createHeader(order));
              else headers.add(hds.get(0));
            }
            synchronize(headers, success, fail);
          }
        } catch (SQLException | JsonServerException | JSONException e1) {
          Log.e(TAG, "Exception", e1);
          fail.run(null);
        }
      }
    }, Task.FETCH_ORDER);
    rt.execute(SystemURL.GET_ORDERS_BY_ORDER_NUMBER.getUrl(orderNumber));
  }

  private static void synchronize(List<OrderCommandHeader> headers, final CallbackSuccess success, CallbackFail fail) {
    new Synchronizer(headers, new CallbackSuccess() {
      @Override
      public void run() {
        Sys.log("Successful headers");
        success.run();
      }
    }, new RedoCallbackFail(fail), false).synchronize();
  }

  private static void synchronize(OrderCommandHeader header, boolean print, final CallbackSuccess callback) {
    new Synchronizer(header, new CallbackSuccess() {
      @Override
      public void run() {
        Sys.log("Successful header");
        callback.run();
      }
    }, new CallbackFail<Synchronizer.SynchronizerListener>() {
      @Override
      public void run(Synchronizer.SynchronizerListener l) {
        new RedoCallbackFail().run(l);
        callback.run();
      }
    }, print).synchronize();
  }

  private static class RedoCallbackFail implements CallbackFail<Synchronizer.SynchronizerListener> {
    private CallbackFail fail;
    public RedoCallbackFail(CallbackFail fail) {this.fail = fail;}
    public RedoCallbackFail() { this(CallbackFail.NULL); }
    @Override
    public void run(Synchronizer.SynchronizerListener listener) {
      Sys.log("Failed header");
      fail.run(listener);
      INSTANCE.addTask(listener.getHeader(), listener.isPrint(), FastSellingServicePriority.NORMAL);
    }
  }

  private static class Work {
    private OrderCommandHeader header;
    private boolean print;

    public Work(OrderCommandHeader header, boolean print) {
      this.header = header;
      this.print = print;
    }
  }
}

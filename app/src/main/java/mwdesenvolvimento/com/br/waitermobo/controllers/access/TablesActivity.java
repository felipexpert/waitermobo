package mwdesenvolvimento.com.br.waitermobo.controllers.access;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling.FastSellingActivity;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.BusyTablesResolver;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingServiceImpl;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.OrderNumberControllable;

/**
 * Created by geppetto on 13/08/15.
 */
public class TablesActivity extends FragmentActivity implements CallbackSuccess{
  private static final String TAG = TablesActivity.class.getSimpleName();
  private static final int COLUMNS = 5;
  private Map<Integer, Button> buttons = new HashMap<>();
  private BusyTablesResolver resolver;
  private LinearLayout body;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_tables);
    body = (LinearLayout) findViewById(R.id.body);
    prepareButtons(Sys.getInstance().getTotalOrderNumbers());
    resolver = new BusyTablesResolver(getBaseContext(), this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    resolver.prepare();
  }

  private void prepareButtons(int amount) {
    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT
    );
    final LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.WRAP_CONTENT,
        1f / COLUMNS
    );
    for(int i = 1; i <= amount; i+=COLUMNS) {
      LinearLayout layout = new LinearLayout(this);
      layout.setLayoutParams(layoutParams);
      layout.setOrientation(LinearLayout.HORIZONTAL);
      for(int j = 0, number = i; j < COLUMNS && number <= amount; j++, number++) {
        Button button = new Button(this);
        button.setText(Integer.toString(number));
        button.setLayoutParams(buttonParams);
        final int number1 = number;
        new FastSellingServiceImpl(new OrderNumberControllable() {
          @Override
          public int getOrderNumber() {
            return number1;
          }

          @Override
          public void showIdentifiers(int orderNumber) {
            Intent i = new Intent(TablesActivity.this, IdentifierActivity.class);
            i.putExtra("orderNumber", orderNumber);
            startActivity(i);
          }

          @Override
          public void showOrder(int headerId) {
            Intent i = new Intent(TablesActivity.this, FastSellingActivity.class);
            i.putExtra("headerId", headerId);
            startActivity(i);
          }

          @Override
          public void close() {
            finish();
          }
        }, getBaseContext(), button, true, true).prepare();
        layout.addView(button);
        buttons.put(number, button);
      }
      body.addView(layout);
    }
  }

  @Override
  public void run() {
    Set<Integer> busy = resolver.getBusyOrderNumbers();
    for(Integer orderNumber : busy) if(buttons.containsKey(orderNumber))
      buttons.get(orderNumber).setTextColor(Color.RED);
  }
}

package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.OrderItem;
import mwdesenvolvimento.com.br.waitermobo.models.Product;
import mwdesenvolvimento.com.br.waitermobo.models.Waiter;

/**
 * Created by geppetto on 30/07/15.
 */
public class FastSellingBridge {
  private static final String TAG = FastSellingBridge.class.getName();

  private OrderCommandHeader header;

  private VirtualOrder virtualOrder;

  public FastSellingBridge(OrderCommandHeader header) {
    this.header = header;
  }

  public FastSellingBridge() {}

  public String getObs() {
    if(virtualOrder == null) virtualize();
    return virtualOrder.getObs();
  }
  public Waiter getWaiter() {
    if(virtualOrder == null) virtualize();
    return virtualOrder.getWaiter();
  }
  public Set<Item> getItems() {
    if(virtualOrder == null) virtualize();
    return virtualOrder.getItems();
  }
  public Item getItem(int displayOrder) {
    if(virtualOrder == null) virtualize();
    for(Item i : getItems()) if(i.getDisplayOrder() == displayOrder) return i;
    throw new IllegalArgumentException("Invalid displayOrder");
  }
  public boolean hasProductCode(String code) {
    return prod(code) != null;
  }
  public Set<Item> addItem(String code, double amount, double price) throws ProductInvalidCodeException {
    if(!hasProductCode(code)) throw new ProductInvalidCodeException();
    int displayOrder = virtualOrder.getItems().size() > 0 ? virtualOrder.getItems().last().getDisplayOrder() + 1 : 1;
    if(virtualOrder == null) virtualize();
    createCommand(OrderCommandType.ADD_ITEM,
            "code", code,
            "amount", Double.toString(amount),
            "price", Double.toString(price),
            "displayOrder",
            Integer.toString(displayOrder));
    virtualize();
    return virtualOrder.getItems();
  }
  public Set<Item> addItem(String code, double amount) throws ProductInvalidCodeException {
    if(!hasProductCode(code)) throw new ProductInvalidCodeException();
    return addItem(code, amount, prod(code).getPrice());
  }
  public String setObs(String obs) {
    createCommand(OrderCommandType.CHANGE_OBS, "obs", obs);
    virtualize();
    return virtualOrder.getObs();
  }
  public Set<Item> setItemAmount(Item item, double amount) {
    createCommand(OrderCommandType.CHANGE_ITEM_AMOUNT,
            "displayOrder", Integer.toString(item.getDisplayOrder()),
            "amount", Double.toString(amount));
    virtualize();
    return virtualOrder.getItems();
  }
  public Set<Item> removeItem(Item item) {
    createCommand(OrderCommandType.REMOVE_ITEM,
            "displayOrder", Integer.toString(item.getDisplayOrder()));
    virtualize();
    return virtualOrder.getItems();
  }

  public VirtualOrder virtualize() {
    return virtualOrder = VirtualOrderUtils.virtualize(header);
  }
  public VirtualOrder getVirtualOrder() {
    if(virtualOrder == null) virtualize();
    return virtualOrder;
  }

  public String getIdentifier() {
    if(virtualOrder == null) virtualize();
    return virtualOrder.getIdentifier();
  }

  public String changeIdentifier(String identifier) {
    String r = null;
    try {
      header.setIdentifier(identifier);
      Dao<OrderCommandHeader, Integer> headerDAO = Database.getDAO(OrderCommandHeader.class);
      headerDAO.update(header);
      virtualize();
      r = virtualOrder.getIdentifier();
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    return r;
  }

  public OrderCommandHeader getHeader() {
    return header;
  }

  public void setHeader(OrderCommandHeader header) {
    this.header = header;
  }

  public void setHeader(int headerId) throws SQLException {
    Dao<OrderCommandHeader, Integer> dao = Database.getDAO(OrderCommandHeader.class);
    this.header = dao.queryForId(headerId);
  }

  public int getOrderNumber() {
    if(virtualOrder == null) virtualize();
    return virtualOrder.getOrderNumber();
  }

  private void createCommand(OrderCommandType type, String... pv) {
    Sys.assert_(pv.length % 2 == 0, "pv must be a parameter-value array!");
    OrderCommand oc = new OrderCommand();
    oc.setOrderCommandHeader(header);
    oc.setOrderCommandType(type);
    List<Parameter> ps = new ArrayList<>();
    for(int i = 0; i < pv.length; i += 2) ps.add(new Parameter(0, oc, pv[i], pv[i + 1]));
    oc.setParameters(ps);
    try {
      Dao<OrderCommand, Integer> ocDao = Database.getDAO(OrderCommand.class);
      ocDao.create(oc);
      Dao<Parameter, Integer> pDao = Database.getDAO(Parameter.class);
      for(Parameter p : ps) pDao.create(p);
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
  }

  private Product prod(String code) {
    Product p = null;
    Dao<Product, Integer> dao = Database.getDAO(Product.class);
    try {
      List<Product> prods = dao.queryForEq("code", code);
      if(prods.size() > 0) p = prods.get(0);
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    return p;
  }
}

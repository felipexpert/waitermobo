package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;

import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.OrderItem;

/**
 * Created by geppetto on 04/08/15.
 */
@DatabaseTable(tableName="tbl_order_command_header")
public class OrderCommandHeader {
  @DatabaseField(generatedId = true)
  private int _id;
  @DatabaseField(foreign = true, foreignAutoCreate = true,
      foreignAutoRefresh=true, maxForeignAutoRefreshLevel=1)
  private Order order;
  @DatabaseField
  private int orderNumber;
  @DatabaseField
  private String identifier;
  @ForeignCollectionField(eager = true)
  private Collection<OrderCommand> commands = new ArrayList<>();

  public OrderCommandHeader() {}

  public OrderCommandHeader(int _id, Order order, int orderNumber, String identifier) {
    this._id = _id;
    this.order = order;
    this.orderNumber = orderNumber;
    this.identifier = identifier;
  }

  public int get_id() {
    return _id;
  }

  public void set_id(int _id) {
    this._id = _id;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(int orderNumber) {
    this.orderNumber = orderNumber;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public Collection<OrderCommand> getCommands() {
    return commands;
  }

  public void setCommands(Collection<OrderCommand> commands) {
    this.commands = commands;
  }

  @Override
  public boolean equals(Object o) {
    if(o == this) return true;
    if(!(o instanceof OrderCommandHeader)) return false;
    return ((OrderCommandHeader) o).get_id() == get_id();
  }

  @Override
  public int hashCode() {
    return get_id();
  }

  @Override
  public String toString() {
    return "Header: {id: (" + _id + "), order: (" + order + "), orderNumber: (" + orderNumber + "), identifier: (" + identifier + ")}";
  }
}

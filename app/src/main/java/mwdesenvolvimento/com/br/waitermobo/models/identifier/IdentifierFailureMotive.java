package mwdesenvolvimento.com.br.waitermobo.models.identifier;

/**
 * Created by geppetto on 09/08/15.
 */
public enum IdentifierFailureMotive {
  UPDATING_IDENTIFIER, NEW_IDENTIFIER, CONNECTION;
}

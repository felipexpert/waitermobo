package mwdesenvolvimento.com.br.waitermobo.miscellaneous;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by geppetto on 04/05/15.
 */
public class RealSubject implements Subject {
  private Map<String, Object> data = new HashMap<>();
  private List<Listener> listeners = new ArrayList<>();

  public void addListener(Listener l) {
    listeners.add(l);
  }

  public boolean removeListener(Listener l) {
    return listeners.remove(l);
  }

  public void setData(String key, Object data) {
    this.data.put(key, data);
  }

  @Override
  public void notifyAllListeners() {
    for(Listener l : listeners)
      l.update(this.data);
  }
}

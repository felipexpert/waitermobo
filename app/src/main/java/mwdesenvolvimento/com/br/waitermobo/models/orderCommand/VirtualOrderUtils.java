package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import android.util.Log;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.OrderItem;
import mwdesenvolvimento.com.br.waitermobo.models.Product;

/**
 * Created by geppetto on 04/08/15.
 */
public class VirtualOrderUtils {
  private static final String TAG = VirtualOrderUtils.class.getSimpleName();
  private VirtualOrderUtils() {}
  public static Map<OrderCommandHeader, VirtualOrder> virtualOrdersByOrderNumber(int orderNumber) {
    Map<OrderCommandHeader, VirtualOrder> virtualOrders = new HashMap<>();
    try {
      Dao<OrderCommandHeader, Integer> headerDAO = Database.getDAO(OrderCommandHeader.class);
      List<OrderCommandHeader> headers = headerDAO.queryForEq("orderNumber", orderNumber);
      for(OrderCommandHeader header : headers)
          virtualOrders.put(header, virtualize(header));
      Dao<Order, Integer> orderDAO = Database.getDAO(Order.class);
      A:
      for(Order order : orderDAO.queryForEq("orderNumber", orderNumber)) {
        for(OrderCommandHeader header : headers)
          if (header.getOrder() != null && header.getOrder().get_id() == order.get_id())
            continue A;
        OrderCommandHeader header = createHeader(order);
        virtualOrders.put(header, virtualOrder(order));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return virtualOrders;
  }

  public static VirtualOrder virtualOrder(Order order) {
    VirtualOrder vo = new VirtualOrder();
    vo.setOrderNumber(order.getOrderNumber());
    vo.setOrderId(order.get_id());
    vo.setIdentifier(order.getIdentifier());
    vo.setWaiter(order.getWaiter() != null ? order.getWaiter() : null);
    for(OrderItem oi : order.getOrderItemCollection()) {
      if (oi.getDisabled()) continue;
      vo.getItems().add(new Item(oi.get_id(), oi.getProduct(), oi.getPrice(), oi.getAmount(), oi.getDisplayOrder(), true));
    }
    vo.setObs(order.getObs());
    return vo;
  }

  public static VirtualOrder virtualize(OrderCommandHeader header) {
    Order order = header.getOrder();
    int orderNumber = header.getOrderNumber();
    String identifier = header.getIdentifier();
    VirtualOrder vo = null;
    if(order != null) {
      vo = virtualOrder(order);
      if (header.getIdentifier() != null &&
          !header.getIdentifier().equalsIgnoreCase(order.getIdentifier())) {
        vo.setIdentifier(header.getIdentifier());
        vo.setChanged(true);
      }
    } else {
      vo = new VirtualOrder();
      if(header.getIdentifier() != null) {
        vo.setIdentifier(header.getIdentifier());
        vo.setChanged(true);
      }
    }
    vo.setOrderNumber(header.getOrderNumber());
    try {
      Dao<OrderCommand, Integer> dao = Database.getDAO(OrderCommand.class);
      List<OrderCommand> commands = dao.queryForEq("orderCommandHeader_id", header.get_id());
      if(commands.size() > 0) {
        vo.setChanged(true);
        for(OrderCommand oc : commands) {
          Map<String, String> parameters = new HashMap<>();
          for (Parameter p : oc.getParameters()) parameters.put(p.getParameter(), p.getValue());
          switch (oc.getOrderCommandType()) {
            case ADD_ITEM:
              Dao<Product, Integer> dao2 = Database.getDAO(Product.class);
              List<Product> prods = dao2.queryForEq("code", parameters.get("code"));
              vo.getItems().add(new Item(prods.get(0), Double.parseDouble(parameters.get("price")),
                  Double.parseDouble(parameters.get("amount")),
                  Integer.parseInt(parameters.get("displayOrder")), false));
              break;
            case CHANGE_ITEM_AMOUNT:
              for(Item i : vo.getItems()) if(i.getDisplayOrder() == Integer.parseInt(parameters.get("displayOrder"))) {
                i.setAmount(Double.parseDouble(parameters.get("amount")));
                break;
              }
              break;
            case REMOVE_ITEM:
              for(Item i : vo.getItems()) if(i.getDisplayOrder() == Integer.parseInt(parameters.get("displayOrder"))) {
                vo.getItems().remove(i);
                break;
              }
              break;
            case CHANGE_OBS:
              vo.setObs(parameters.get("obs"));
              break;
            default:
              Sys.assert_(false, "Invalid command type!");
          }
        }
      }
      Sys.assert_(Sys.getInstance().getWaiter() != null, "System waiter is null!");
      if(vo.getWaiter() == null) vo.setWaiter(Sys.getInstance().getWaiter());
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    return vo;
  }

  public static OrderCommandHeader createHeader(int orderNumber) {
    return createHeader(null, orderNumber, null);
  }

  public static OrderCommandHeader createHeader(int orderNumber, String identifier) {
    return createHeader(null, orderNumber, identifier);
  }

  public static OrderCommandHeader createHeader(String identifier) {
    return createHeader(-1, identifier);
  }

  public static OrderCommandHeader createHeader(Order order) {
    return createHeader(order, order.getOrderNumber(), order.getIdentifier());
  }

  public static List<OrderCommandHeader> getPendentHeaders() {
    List<OrderCommandHeader> pendent = new ArrayList<>();
    try {
      Dao<OrderCommandHeader, Integer> dao = Database.getDAO(OrderCommandHeader.class);
      List<OrderCommandHeader> headers = dao.queryForAll();
      for(OrderCommandHeader h : headers) if(virtualize(h).isChanged()) pendent.add(h);
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    return pendent;
  }

  private static OrderCommandHeader createHeader(Order order, int orderNumber, String identifier) {
    OrderCommandHeader header = new OrderCommandHeader();
    header.setOrder(order);
    header.setOrderNumber(orderNumber);
    header.setIdentifier(identifier);
    try {
      Dao<OrderCommandHeader, Integer> dao = Database.getDAO(OrderCommandHeader.class);
      dao.create(header);
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    return header;
  }
}
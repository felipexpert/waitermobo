package mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.Item;

/**
 * Created by geppetto on 29/06/15.
 */
public class ItemAdapter extends ArrayAdapter<Item> {
  private static final String TAG = ItemAdapter.class.getSimpleName();
  private Context context;
  private List<Item> items;
  public ItemAdapter(Context context, int resource, List<Item> items) {
    super(context, resource, items);
    this.context = context;
    this.items = items;
    Log.d(TAG, "ItemAdapter was constructed");
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // 1. Create inflater
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // 2. Get rowView from inflater
    View rowView = inflater.inflate(R.layout.ac_order_item, parent, false);

    // 3. Get the two text view from the rowView
    TextView lblCode = (TextView) rowView.findViewById(R.id.lblCode);
    TextView lblName = (TextView) rowView.findViewById(R.id.lblName);
    TextView lblAmount = (TextView) rowView.findViewById(R.id.lblAmount);
    TextView lblPrice = (TextView) rowView.findViewById(R.id.lblPrice);
    TextView lblSubTotal = (TextView) rowView.findViewById(R.id.lblSubTotal);

    // 4. Set the text for textView
    Item i = items.get(position);
    lblCode.setText(i.getProduct().getCode() + " - ");
    String prodName = i.getProduct().getName();
    if(prodName.length() > 22)
      prodName = prodName.substring(0, 19) + "...";
    lblName.setText(prodName);
    lblName.setTextColor(Color.BLUE);
    lblAmount.setTextColor(Color.BLUE);
    if(!i.isOnServer()) {
      lblName.setTextColor(Color.RED);
      lblAmount.setTextColor(Color.RED);
    }
    lblAmount.setText(Sys.getInstance().amountFormat(i.getAmount()));
    lblPrice.setText(Sys.getInstance().currencyFormat(i.getPrice()));
    lblSubTotal.setText(Sys.getInstance().currencyFormat(i.getAmount() * i.getPrice()));

    // 5. return rowView
    return rowView;
  }
}
package mwdesenvolvimento.com.br.waitermobo.controllers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import java.util.List;

import br.com.mwdesenvolvimento.mylibrary.server.Initializer;
import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.access.KeypadActivity;
import mwdesenvolvimento.com.br.waitermobo.controllers.access.TablesActivity;
import mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling.FastSellingActivity;
import mwdesenvolvimento.com.br.waitermobo.controllers.access.IdentifierActivity;
import mwdesenvolvimento.com.br.waitermobo.controllers.product.CategoriesActivity;
import mwdesenvolvimento.com.br.waitermobo.controllers.waiter.HostnameActivity;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.DatabaseHelper;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrderUtils;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingService;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingServiceImpl;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingServicePriority;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.OrderNumberControllable;
import mwdesenvolvimento.com.br.waitermobo.models.syncAll.ConnectionTester;


public class MainActivity extends ActionBarActivity implements OrderNumberControllable {
  private static final String TAG = MainActivity.class.getSimpleName();
  private static boolean basicResources;

  private Button btnPendent;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Initializer.INSTANCE.init(Database.getInstance(), new DatabaseHelper(this));
    Log.d(TAG, "onCreate from MainActivity has been called");
    if(!basicResources) {
      Database.getInstance().init(getBaseContext());
      Sys.getInstance().init(this);
      basicResources = true;
      FastSellingService.init();
    } else {
      FastSellingService.init();
    }
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_main);
    Button btnCats = (Button) findViewById(R.id.btnCheckCategories);
    btnCats.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent(MainActivity.this, CategoriesActivity.class);
        startActivity(i);
      }
    });
    Button btnKeypad = (Button) findViewById(R.id.btnKeypad);
    btnKeypad.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getBaseContext(), KeypadActivity.class));
      }
    });
    Button btnTables = (Button) findViewById(R.id.btnTables);
    btnTables.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getBaseContext(), TablesActivity.class));
      }
    });
    Button btnLoose = (Button) findViewById(R.id.btnLoose);
    new FastSellingServiceImpl(this, getBaseContext(), btnLoose, true, false).prepare();
    btnPendent = (Button) findViewById(R.id.btnPendent);
    btnPendent.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        btnPendent.setVisibility(View.INVISIBLE);
        List<OrderCommandHeader> headers = VirtualOrderUtils.getPendentHeaders();
        for(OrderCommandHeader h : headers) FastSellingService.getInstance().addTask(h, false, FastSellingServicePriority.NORMAL);
        Toast.makeText(getBaseContext(), getString(R.string.synchronizing), Toast.LENGTH_LONG).show();
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    btnPendent.setVisibility(View.INVISIBLE);
    ConnectionTester.test(new CallbackSuccess() {
      @Override
      public void run() {
        Log.d(TAG, "talking to server");
        List<OrderCommandHeader> headers = VirtualOrderUtils.getPendentHeaders();
        if(headers.size() > 0) {
          btnPendent.setVisibility(View.VISIBLE);
          btnPendent.setText(String.format(getString(R.string.pendent), headers.size()));
        }
      }
    }, new CallbackFail<Object>() {
      @Override
      public void run(Object o) {
        Toast.makeText(getBaseContext(), getString(R.string.offlineMsn), Toast.LENGTH_LONG).show();
      }
    });
  }

  public void startWaiterActivity() {
    startActivity(new Intent(getBaseContext(), SystemActivity.class));
    startActivity(new Intent(getBaseContext(), HostnameActivity.class));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    switch (id) {
      case R.id.action_settings:
        Intent i = new Intent(MainActivity.this, SystemActivity.class);
        startActivity(i);
        break;
      case R.id.action_exit:
        exitDialog();
        break;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onDestroy() {
    FastSellingService.end();
    super.onDestroy();
    //Database.getDatabase().close();
  }

  @Override
  public int getOrderNumber() {
    return -1;
  }

  @Override
  public void showIdentifiers(int orderNumber) {
    Intent i = new Intent(this, IdentifierActivity.class);
    i.putExtra("orderNumber", orderNumber);
    startActivity(i);
  }

  @Override
  public void showOrder(int headerId) {
    Intent i = new Intent(this, FastSellingActivity.class);
    i.putExtra("headerId", headerId);
    startActivity(i);
  }

  @Override
  public void close() {
    finish();
  }

  @Override
  public void onBackPressed() {
    exitDialog();
  }

  public void exitDialog() {
    new AlertDialog.Builder(this)
        .setTitle(R.string.confirmation)
        .setMessage(R.string.exitMessage)
        .setPositiveButton(R.string.exactly, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            finish();
          }
        })
        .setNegativeButton(R.string.back, null)
        .show();
  }
}

package mwdesenvolvimento.com.br.waitermobo;

import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;

/**
 * Created by geppetto on 04/05/15.
 */
public enum SystemURL {
  PRODUCTS("mobile/getProds"),
  GET_ORDERS("mobile/getOrders"),
  ONGOING_ORDERS("mobile/ongoingOrders"),
  GET_ORDERS_BY_ORDER_NUMBER("mobile/getOrdersByOrderNumber"),
  TEST_CONNECTION("mobile/test"),
  GET_WAITERS("mobile/getWaiters"),
  SYNC_ORDER("mobile/syncOrder"),
  PRINT("mobile/printOrder"),
  CAN_USE_IDENTIFIER("sale/canUseIdentifierAjax"),
  CAN_USE_IDENTIFIERS("sale/canUseIdentifiersAjax"),
  TABLES_AMOUNT("mobile/tablesAmount");

  private String part;

  SystemURL(String part) {
    this.part = part;
  }

  public String getUrl() {
    return Sys.getInstance().getBaseUrl() + part;
  }

  public String getUrl(String sufix) {
    return Sys.getInstance().getBaseUrl() + part + "/" + sufix;
  }

  public String getUrl(long id) {
    return getUrl(Long.toString(id));
  }
}
package mwdesenvolvimento.com.br.waitermobo.models;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;

/**
 * Created by geppetto on 07/05/15.
 */
public class OrderUtil {
  private OrderUtil() {}

  public static Order prepareOrder(int orderNumber) {
    Order order = null;
    List<Order> orders = null;
    Dao<Order, Integer> dao = Database.getDAO(Order.class);
    try {

      PreparedQuery<Order> pq = dao.queryBuilder()
          .where()
          .eq("orderNumber", orderNumber)
          .and()
          .isNull("closingDate")
          .prepare();
      orders = dao.query(pq);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    if (orders != null && orders.size() > 0) {
      List<Order> nullOrders = new ArrayList<>();
      Order validOne = null;
      for(Order o : orders) {
        nullOrders.add(o);
      }
      if(validOne != null) {
        try {
          dao.delete(nullOrders);
        } catch (SQLException e) {
          e.printStackTrace();
        }
      } else {
        validOne = orders.get(0);
      }
      order = validOne;
    }
    return order;
  }
}

package mwdesenvolvimento.com.br.waitermobo.miscellaneous;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import mwdesenvolvimento.com.br.waitermobo.controllers.MainActivity;
import mwdesenvolvimento.com.br.waitermobo.models.Msn;
import mwdesenvolvimento.com.br.waitermobo.models.Waiter;

/**
 * Created by mindware on 09/03/15.
 */
public class Sys {
  private static final String TAG = Sys.class.getSimpleName();
  private static final Sys INSTANCE =  new Sys();
  private Waiter waiter;
  private String hostname;
  private int totalOrderNumbers;
  private Locale locale = new Locale("pt", "BR");
  private NumberFormat amount = NumberFormat.getInstance(locale);
  private NumberFormat currency = NumberFormat.getInstance(locale);
  private boolean initialized;
  private HashMap<String, Subject> subjects = new HashMap<>();
  //public static final String BASE_URL = "http://192.168.1.101/myerp/igniter/";

  // Singleton
  private Sys() {
    amount.setMinimumFractionDigits(3);
    amount.setMaximumFractionDigits(3);
    currency.setMinimumFractionDigits(2);
    currency.setMaximumFractionDigits(2);
  }

  public static Sys getInstance() {
    return INSTANCE;
  }

  public Waiter getWaiter() {
    return waiter;
  }

  public Locale getLocale() {
    return locale;
  }

  public String currencyFormat(double money) {
    return currency.format(money);
  }

  public String amountFormat(double amount) {
    return this.amount.format(amount);
  }

  public int getTotalOrderNumbers() { return totalOrderNumbers; }
  public String getHostname() { return hostname; }
  public static String getBaseUrl(String hostname) { return "http://" + hostname + "/"; }
  public String getBaseUrl() { return getBaseUrl(getHostname()); }
  public void setWaiter(Waiter waiter) {
    Database.getDatabase().execSQL("update " + Database.SYSTEMS_TABLE_NAME
        + " set id_waiter = ?, "
        + " name_waiter = ?;", new Object[] {waiter.get_id(), waiter.getName()});
    this.waiter = waiter;
  }

  public void setHostname(String hostname) {
    Database.getDatabase().execSQL("update " + Database.SYSTEMS_TABLE_NAME
            + " set hostname = ?;", new Object[] {hostname});
    this.hostname = hostname;
  }

  public void setTotalOrderNumbers(int totalOrderNumbers) {
    Database.getDatabase().execSQL("update " + Database.SYSTEMS_TABLE_NAME
            + " set total_order_numbers = ?;", new Object[] {totalOrderNumbers});
    this.totalOrderNumbers = totalOrderNumbers;
  }

  public void init(MainActivity main) {
    Log.d(TAG, "System will be initialized");
    if(initialized) throw new IllegalStateException("This resource is already initialized");
    try {
      SQLiteDatabase db = Database.getDatabase();
      Cursor c = db.rawQuery("select hostname, id_waiter, name_waiter, total_order_numbers from " + Database.SYSTEMS_TABLE_NAME, null);
      String hostname;
      long idWaiter = 0;
      String nameWaiter = null;
      int totalOrderNumbers;
      c.moveToFirst();
      hostname = c.getString(0);
      idWaiter = c.getLong(1);
      nameWaiter = c.getString(2);
      this.totalOrderNumbers = c.getInt(3);
      if (idWaiter > 0) {
        this.hostname = hostname;
        waiter = Database.getDAO(Waiter.class, Long.class).queryForId(idWaiter);
        initialized = true;
      } else {
        Log.d(TAG, "B");
        main.startWaiterActivity();
      }
    } catch(SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean isInitialized()  {
    return initialized;
  }

  public Subject addSubject(String name) {
    Subject subject = new RealSubject();
    subjects.put(name, subject);
    return subject;
  }

  public Subject addSubject(Class<?> clazz) {
    return addSubject(clazz.getName());
  }

  public Subject removeSubject(Class<?> clazz) {
    return subjects.remove(clazz.getName());
  }

  public Subject removeSubject(String name) {
    return subjects.remove(name);
  }

  public boolean listenToSubject(String name, Listener listener) {
    if(subjects.containsKey(name)) {
      subjects.get(name).addListener(listener);
      return true;
    }
    return false;
  }

  public boolean listenToSubject(Class<?> clazz, Listener listener) {
    return listenToSubject(clazz.getName(), listener);
  }

  public boolean setDataAndNotify(String subjectName, Map<String, Object> data) {
    Subject s = subjects.get(subjectName);
    if(s == null) return false;
    Set<String> keys = data.keySet();
    for(String key : keys)
      s.setData(key, data.get(key));
    s.notifyAllListeners();
    return true;
  }

  public boolean setDataAndNotify(Class<?> clazz, Map<String, Object> data) {
    return setDataAndNotify(clazz.getName(), data);
  }
  public static void assert_(boolean test, String ... msn) {
    StringBuilder sb = new StringBuilder();
    for(String s : msn) sb.append(sb).append(' ');
    if(!test) throw new AssertionError(sb.toString());
  }
  public static void log(Object... objects) {
    StringBuilder sb = new StringBuilder();
    for(Object o : objects) sb.append(o).append(' ');
    Dao<Msn, Integer> msnDAO = Database.getDAO(Msn.class);
    Msn msn = new Msn(0, sb.toString());
    try {
      msnDAO.create(msn);
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
  }
  public static void clearAllLogs() {
    Dao<Msn, Integer> msnDAO = Database.getDAO(Msn.class);
    try {
      msnDAO.delete(msnDAO.queryForAll());
    } catch (SQLException e) {
      Log.e(TAG, "Exception", e);
    }
  }
 }

package mwdesenvolvimento.com.br.waitermobo.controllers.waiter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.models.Waiter;

/**
 * Created by mindware on 23/03/15.
 */
public class WaiterAdapter extends ArrayAdapter<Waiter> {
  private Context context;
  private List<Waiter> waiters;
  public WaiterAdapter(Context context, int resource, List<Waiter> waiters) {
    super(context, resource, waiters);
    this.context = context;
    this.waiters = waiters;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // 1. Create inflater
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // 2. Get rowView from inflater
    View rowView = inflater.inflate(R.layout.ac_waiter_item, parent, false);

    // 3. Get the two text view from the rowView
    TextView txtId = (TextView) rowView.findViewById(R.id.txtId);

    // 4. Set the text for textView
    Waiter w  = waiters.get(position);
    txtId.setText(w.get_id() + " - " + w.getName());

    // 5. return rowView
    return rowView;
  }
}

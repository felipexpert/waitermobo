package mwdesenvolvimento.com.br.waitermobo.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by mindware on 09/03/15.
 */
@DatabaseTable(tableName="tbl_waiter")
public class Waiter {
  @DatabaseField(id = true)
  private long _id;
  @DatabaseField
  private String name;

  public Waiter()  {}

  public Waiter(long _id, String name)  {
    this._id = _id;  this.name = name;
  }

  public long get_id() {
    return _id;
  }

  public void set_id(long _id) {
    this._id = _id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return _id + " - " + name;
  }
}

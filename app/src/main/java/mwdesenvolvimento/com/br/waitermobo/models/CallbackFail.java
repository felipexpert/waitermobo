package mwdesenvolvimento.com.br.waitermobo.models;

import java.util.Objects;

/**
 * Created by geppetto on 09/08/15.
 */
public interface CallbackFail <T> {
  CallbackFail NULL = new CallbackFail<Object>() {
    @Override
    public void run(Object o) {}
  };

  void run(T t);
}

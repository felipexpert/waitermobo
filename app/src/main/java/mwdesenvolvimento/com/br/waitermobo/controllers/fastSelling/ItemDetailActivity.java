package mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.SQLException;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.FastSellingBridge;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.Item;

/**
 * Created by geppetto on 29/06/15.
 */
public class ItemDetailActivity extends Activity {
  private static final String TAG = ItemDetailActivity.class.getSimpleName();
  private int displayOrder;
  private FastSellingBridge bridge;
  private TextView lblTotal;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_item_detail);
    try {
      Bundle extras = getIntent().getExtras();
      displayOrder = extras.getInt("displayOrder");
      int headerId = extras.getInt("headerId");
      bridge = new FastSellingBridge();
      bridge.setHeader(headerId);
      Log.d(TAG, "" + (bridge.getHeader() != null));
      final Item item = item();
      TextView lblProdCode = (TextView) findViewById(R.id.lblProdCode);
      lblProdCode.setText(item.getProduct().getCode() + " - "
          + item.getProduct().getName());
      TextView lblProdPrice = (TextView) findViewById(R.id.lblPrice);
      lblProdPrice.setText(Sys.getInstance().currencyFormat(item.getPrice()));
      lblTotal = (TextView) findViewById(R.id.lblTotal);
      updateTotal();
      final EditText txtAmount = (EditText) findViewById(R.id.txtAmount);
      txtAmount.setText(Double.toString(item.getAmount()));
      txtAmount.addTextChangedListener(new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before, int count) {}
        public void afterTextChanged(Editable s) {
          try {
            double amount = Double.parseDouble(s.toString());
            bridge.setItemAmount(item, amount);
            updateTotal();
          } catch (NumberFormatException _) {}
        }
      });
      Button btnPlus = (Button) findViewById(R.id.btnPlus);
      btnPlus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          try {
            double amount = Double.parseDouble(txtAmount.getText().toString()) + 1d;
            txtAmount.setText(Double.toString(amount));
            bridge.setItemAmount(item, amount);
            updateTotal();
          } catch (NumberFormatException _) {}
        }
      });
      Button btnMinus = (Button) findViewById(R.id.btnMinus);
      btnMinus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          try {
            double amount = Double.parseDouble(txtAmount.getText().toString()) - 1d;
            txtAmount.setText(Double.toString(amount));
            bridge.setItemAmount(item, amount);
            updateTotal();
          } catch(NumberFormatException _) {}
        }
      });
      Button btnDel = (Button) findViewById(R.id.btnDel);
      btnDel.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          bridge.removeItem(item);
          ItemDetailActivity.this.finish();
        }
      });
    } catch(SQLException e) {
      Log.e(TAG, "Exception", e);
    }
  }

  private void updateTotal() {
    Item i = item();
    lblTotal.setText(Sys.getInstance().currencyFormat(i.getAmount() * i.getPrice()));
  }

  private Item item() {
    return bridge.getItem(displayOrder);
  }
}
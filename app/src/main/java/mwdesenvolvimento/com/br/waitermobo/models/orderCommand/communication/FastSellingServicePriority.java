package mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication;

/**
 * Created by geppetto on 05/08/15.
 */
public enum FastSellingServicePriority {
  NORMAL, HIGH;
}

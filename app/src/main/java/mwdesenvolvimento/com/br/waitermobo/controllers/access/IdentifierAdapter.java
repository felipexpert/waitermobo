package mwdesenvolvimento.com.br.waitermobo.controllers.access;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;

/**
 * Created by geppetto on 03/08/15.
 */
public class IdentifierAdapter extends ArrayAdapter<OrderCommandHeader> {
  private Context context;
  private List<OrderCommandHeader> orderCommandHeaders;

  public IdentifierAdapter(Context context, int resource, List<OrderCommandHeader> orderCommandHeaders) {
    super(context, resource, orderCommandHeaders);
    this.context = context;
    this.orderCommandHeaders = orderCommandHeaders;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // 1. Create inflater
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // 2. Get rowView from inflater
    View rowView = inflater.inflate(R.layout.ac_order, parent, false);

    // 3. Get the text view from the rowView
    // 4. Set the text for textView
    OrderCommandHeader header = orderCommandHeaders.get(position);
    TextView txtIdentifier = (TextView) rowView.findViewById(R.id.txtIdentifier);
    if (header.getOrder() == null) {
      txtIdentifier.setText(header.getIdentifier() != null ? header.getIdentifier() : String.format(context.getString(R.string.withoutIdentifier), header.get_id()));
      txtIdentifier.setTextColor(Color.RED);
    } else {
      txtIdentifier.setText(header.getIdentifier() != null ? header.getIdentifier() : String.format(context.getString(R.string.withoutIdentifier), header.getOrder().get_id()));
    }

    // 5. return rowView
    return rowView;
  }
}

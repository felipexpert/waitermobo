package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import java.util.List;
import java.util.TreeSet;

import mwdesenvolvimento.com.br.waitermobo.models.OrderItem;
import mwdesenvolvimento.com.br.waitermobo.models.Waiter;

/**
 * Created by geppetto on 26/06/15.
 */
public class VirtualOrder {
  private int orderNumber = -1;
  private long orderId = -1;
  private String identifier;
  private Waiter waiter;
  private TreeSet<Item> items = new TreeSet<>();
  private String obs = "";
  private boolean changed;

  public int getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(int orderNumber) {
    this.orderNumber = orderNumber;
  }

  public long getOrderId() {
    return orderId;
  }

  public void setOrderId(long orderId) {
    this.orderId = orderId;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public Waiter getWaiter() {
    return waiter;
  }

  public void setWaiter(Waiter waiter) {
    this.waiter = waiter;
  }

  public TreeSet<Item> getItems() {
    return items;
  }

  public void setItems(TreeSet<Item> items) {
    this.items = items;
  }

  public String getObs() {
    return obs;
  }

  public void setObs(String obs) {
    this.obs = obs;
  }

  public boolean isChanged() {
    return changed;
  }

  public void setChanged(boolean changed) {
    this.changed = changed;
  }

  @Override
  public String toString() {
    return "VirtualOrder {orderId(" + orderId + "),identifier(" + identifier + "),waiter(" + waiter
            + "),items(" + items + "),obs(" + obs + "),changed(" + changed + ")}";
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(!(o instanceof VirtualOrder)) return false;
    VirtualOrder vo = (VirtualOrder) o;
    boolean itemsEqual = true;
    if(items.size() == vo.items.size()) {
      A:
      for (Item i : items) {
        for(Item i2 : vo.items) if(i.equals(i2)) continue A;
        itemsEqual = false;
        break;
      }
    } else {
      itemsEqual = false;
    }
    return itemsEqual && orderId == vo.orderId
      && (identifier == null && vo.identifier == null ||
            identifier != null && identifier.trim().equalsIgnoreCase(vo.identifier.trim()))
      && obs.equals(vo.obs);
  }

  public double total() {
    double total = 0d;
    for(Item i : items)
      total += i.getAmount() * i.getPrice();
    return total;
  }
}

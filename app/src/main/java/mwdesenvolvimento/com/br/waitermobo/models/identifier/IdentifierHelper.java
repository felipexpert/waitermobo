package mwdesenvolvimento.com.br.waitermobo.models.identifier;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.SystemURL;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;

/**
 * Created by geppetto on 09/08/15.
 */
public class IdentifierHelper {
  private static final String TAG = IdentifierHelper.class.getSimpleName();
  private OrderCommandHeader header;
  private String updatingIdentifier;
  private String newIdentifier;
  private CallbackSuccess success;
  private CallbackFail<IdentifierFailureMotive> fail;

  public IdentifierHelper(OrderCommandHeader header, String updatingIdentifier, String newIdentifier, CallbackSuccess success, CallbackFail<IdentifierFailureMotive> fail) {
    this.header = header;
    this.updatingIdentifier = updatingIdentifier;
    this.newIdentifier = newIdentifier;
    this.success = success;
    this.fail = fail;
  }

  public IdentifierHelper(OrderCommandHeader header, String updatingIdentifier, CallbackSuccess success, CallbackFail<IdentifierFailureMotive> fail) {
    this(header, updatingIdentifier, null, success, fail);
  }

  public IdentifierHelper(String newIdentifier, CallbackSuccess success, CallbackFail<IdentifierFailureMotive> fail) {
    this(null, null, newIdentifier, success, fail);
  }

  public void perform() {
    try {
      if (header != null) {
        PostRequestTask rt = new PostRequestTask(new TaskListener() {
          @Override
          public void act(String result, Task task, Exception e) {
            if (e == null) {
              try {
                JSONObject r = new JSONObject(result);
                boolean c1 = r.getBoolean("canUseIdentifier1");
                boolean c2 = r.getBoolean("canUseIdentifier2");
                if (!c1 && updatingIdentifier != null) fail.run(IdentifierFailureMotive.UPDATING_IDENTIFIER);
                else if (!c2 && newIdentifier != null) fail.run(IdentifierFailureMotive.NEW_IDENTIFIER);
                else success.run();
              } catch (JSONException e2) {
                Log.e(TAG, "Exception", e2);
                fail.run(IdentifierFailureMotive.CONNECTION);
              }
            } else {
              fail.run(IdentifierFailureMotive.CONNECTION);
            }
          }
        });
        JSONObject send = new JSONObject();
        send.put("identifier1", updatingIdentifier);
        send.put("identifier2", newIdentifier);
        if (header.getOrder() != null)
          rt.execute(SystemURL.CAN_USE_IDENTIFIERS.getUrl(header.getOrder().get_id()), send.toString());
        else
          rt.execute(SystemURL.CAN_USE_IDENTIFIERS.getUrl(), send.toString());
      } else {
        PostRequestTask rt = new PostRequestTask(new TaskListener() {
          @Override
          public void act(String result, Task task, Exception e) {
            try {
              JSONObject r = new JSONObject(result);
              boolean cUse = r.getBoolean("canUseIdentifier");
              if (!cUse) fail.run(IdentifierFailureMotive.NEW_IDENTIFIER);
              else success.run();
            } catch (JSONException e2) {
              Log.e(TAG, "Exception", e2);
              fail.run(IdentifierFailureMotive.CONNECTION);
            }
          }
        });
        JSONObject send = new JSONObject();
        send.put("identifier", newIdentifier);
        rt.execute(SystemURL.CAN_USE_IDENTIFIER.getUrl(), send.toString());
      }
    } catch (JSONException e2) {
      Log.e(TAG, "Exception", e2);
      fail.run(IdentifierFailureMotive.CONNECTION);
    }
  }
}

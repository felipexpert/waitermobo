package mwdesenvolvimento.com.br.waitermobo.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import br.com.mwdesenvolvimento.mylibrary.controllers.AndroidDatabaseManager;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.controllers.waiter.HostnameActivity;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.DatabaseHelper;
import mwdesenvolvimento.com.br.waitermobo.models.syncAll.SyncAllTaskListener;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;

import mwdesenvolvimento.com.br.waitermobo.R;

/**
 * Created by mindware on 09/03/15.
 */
public class SystemActivity extends Activity implements TaskListener {
  private static final String TAG = SystemActivity.class.getSimpleName();
  private TextView lblTablesAmount;
  private SyncAllTaskListener syncAllTaskListener = new SyncAllTaskListener(this);
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_system);
    Button btnSetup = (Button) findViewById(R.id.btnSetup);
    btnSetup.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        new AlertDialog.Builder(SystemActivity.this)
            .setTitle(R.string.confirmation)
            .setMessage(R.string.setupMessage)
            .setPositiveButton(R.string.exactly, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(getBaseContext(), HostnameActivity.class);
                startActivity(i);
              }
            })
            .setNegativeButton(R.string.back, null)
            .show();
      }
    });
    lblTablesAmount = (TextView) findViewById(R.id.lblTablesAmount);
    lblTablesAmount.setText(Integer.toString(Sys.getInstance().getTotalOrderNumbers()));
    Button btnManualSync = (Button) findViewById(R.id.btnManualSync);
    btnManualSync.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        new AlertDialog.Builder(SystemActivity.this)
            .setTitle(R.string.confirmation)
            .setMessage(R.string.manualSyncMessage)
            .setPositiveButton(R.string.exactly, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(SystemActivity.this, R.string.wait, Toast.LENGTH_LONG).show();
                syncAllTaskListener.execute();
              }
            })
            .setNegativeButton(R.string.back, null)
            .show();
      }
    });
    Button btnClearLog = (Button) findViewById(R.id.btnClearLog);
    btnClearLog.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Sys.clearAllLogs();
      }
    });
    Button btnDB = (Button) findViewById(R.id.btnDB);
    btnDB.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getBaseContext(), AndroidDatabaseManager.class));
      }
    });
    refreshData();
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshData();
  }

  private void refreshData() {
    Sys conf =  Sys.getInstance();
    TextView lblHostname = (TextView) findViewById(R.id.lblHostname);
    lblHostname.setText(conf.getHostname());
    TextView lblIdWaiter = (TextView) findViewById(R.id.lblIdWaiter);
    lblIdWaiter.setText(Long.toString(conf.getWaiter().get_id()));
    TextView lblNameWaiter = (TextView) findViewById(R.id.lblNameWaiter);
    lblNameWaiter.setText(conf.getWaiter().getName());
  }

  @Override
  public void act(String result, Task task, Exception e) {
    int total = Sys.getInstance().getTotalOrderNumbers();
    lblTablesAmount.setText(Integer.toString(total));
    Toast.makeText(this, R.string.syncSuccessful, Toast.LENGTH_LONG).show();
  }
}

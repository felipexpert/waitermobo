package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandType;

/**
 * Created by geppetto on 26/06/15.
 */
@DatabaseTable(tableName="tbl_order_command")
public class OrderCommand {
  @DatabaseField(generatedId = true)
  private int _id;
  @DatabaseField(foreign = true, foreignAutoCreate = true,
      foreignAutoRefresh=true, maxForeignAutoRefreshLevel=1)
  private OrderCommandHeader orderCommandHeader;
  @DatabaseField
  private OrderCommandType orderCommandType;
  @ForeignCollectionField(eager = true)
  private Collection<Parameter> parameters;
  public OrderCommand() {}

  public OrderCommand(int _id, OrderCommandHeader orderCommandHeader, OrderCommandType orderCommandType, Collection<Parameter> parameters) {
    this._id = _id;
    this.orderCommandHeader = orderCommandHeader;
    this.orderCommandType = orderCommandType;
    this.parameters = parameters;
  }

  public int get_id() {
    return _id;
  }

  public void set_id(int _id) {
    this._id = _id;
  }

  public OrderCommandHeader getOrderCommandHeader() {
    return orderCommandHeader;
  }

  public void setOrderCommandHeader(OrderCommandHeader orderCommandHeader) {
    this.orderCommandHeader = orderCommandHeader;
  }

  public OrderCommandType getOrderCommandType() {
    return orderCommandType;
  }

  public void setOrderCommandType(OrderCommandType orderCommandType) {
    this.orderCommandType = orderCommandType;
  }

  public Collection<Parameter> getParameters() {
    return parameters;
  }

  public void setParameters(Collection<Parameter> parameters) {
    this.parameters = parameters;
  }
}
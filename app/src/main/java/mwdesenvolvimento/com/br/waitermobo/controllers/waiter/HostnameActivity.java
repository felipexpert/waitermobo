package mwdesenvolvimento.com.br.waitermobo.controllers.waiter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.models.syncAll.SyncAllTaskListener;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;

/**
 * Created by geppetto on 01/05/15.
 */
public class HostnameActivity extends Activity implements TaskListener {
    private static final String TAG = HostnameActivity.class.getSimpleName();
    private TextView lblMessage;
    private EditText txtHostname;
    private SyncAllTaskListener syncAllTaskListener = new SyncAllTaskListener(TaskListener.NULL);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_hostname);
        lblMessage = (TextView) findViewById(R.id.lblMessage);
        txtHostname = (EditText) findViewById(R.id.txtHostname);
        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostRequestTask rt = new PostRequestTask(HostnameActivity.this);
                rt.execute(Sys.getBaseUrl(txtHostname.getText().toString()) + "mobile/test");
            }
        });
    }

    @Override
    public void act(String result, Task task, Exception e) {
        if(e == null) {
            try {
                JSONObject json = new JSONObject(result);
                String status = json.getString("_status");
                if(status.equals("success")) {
                    Sys.getInstance().setHostname(txtHostname.getText().toString());
                    Log.d(TAG, Sys.getInstance().getHostname());
                    syncAllTaskListener.execute();
                    startActivity(new Intent(getBaseContext(), SelectWaiter.class));
                    finish();
                    return;
                }
            } catch(JSONException e2) {
                e2.printStackTrace();
            }
        }
        lblMessage.setText(R.string.unreachable);
    }
}

package mwdesenvolvimento.com.br.waitermobo.miscellaneous;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import br.com.mwdesenvolvimento.mylibrary.server.DAOHelper;
import mwdesenvolvimento.com.br.waitermobo.models.Product;

/**
 * Created by mindware on 09/03/15.
 */
public class Database implements DAOHelper {
  public static final String TAG = Database.class.getSimpleName();
  public static final String SYSTEMS_TABLE_NAME = "systems";
  private static final Database INSTANCE = new Database();
  private DatabaseHelper dh;
  private SQLiteDatabase db;
  private Map<String, Dao<?, ?>> daos = new HashMap<>();
  // Singleton
  private Database() {}

  public static Database getInstance() {
    return INSTANCE;
  }

  public static SQLiteDatabase getDatabase()  {
    return getInstance().db;
  }

  public static <T, U> Dao<T, U> getDAO(Class<T> clazz, Class<U> pkClass) {
    return INSTANCE.getDao(clazz, pkClass);
  }

  public static <T> Dao<T, Integer> getDAO(Class<T> clazz) {
    return INSTANCE.getDao(clazz);
  }

  @Override
  public <T> Dao<T, Integer> getDao(Class<T> entityClazz) {
    return getDao(entityClazz, Integer.class);
  }

  @Override
  public <T, U> Dao<T, U> getDao(Class<T> entityClazz, Class<U> pkClass) {
    if(daos.containsValue(entityClazz.getName())) return (Dao<T, U>) daos.get(entityClazz.getName());
    Dao<T, U> dao = null;
    try {
      dao = dh.getDao(entityClazz);
      daos.put(entityClazz.getName(), dao);
    } catch(SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    return dao;
  }

  public void init(Context context) {
    Log.d(TAG, "Called init method from Database");
    if(db != null) throw new IllegalStateException("This resource is already started");
    dh = new DatabaseHelper(context);
    db = dh.getWritableDatabase();
    Log.d(TAG, "Database object has been initialized");
  }

  public void resetAllEntities() {
    dh.resetAllEntities();
  }
}

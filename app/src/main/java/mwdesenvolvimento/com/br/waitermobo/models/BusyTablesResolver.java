package mwdesenvolvimento.com.br.waitermobo.models;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.SystemURL;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrderUtils;

/**
 * Created by geppetto on 13/08/15.
 */
public class BusyTablesResolver implements TaskListener {
  private static final String TAG = BusyTablesResolver.class.getSimpleName();
  private Context context;
  private CallbackSuccess callback;
  private Set<Integer> busyOrderNumbers = new HashSet<>();

  public BusyTablesResolver(Context context, CallbackSuccess callback) {
    this.context = context;
    this.callback = callback;
  }

  public void prepare() {
    PostRequestTask rt = new PostRequestTask(this);
    rt.execute(SystemURL.ONGOING_ORDERS.getUrl());
  }

  @Override
  public void act(String result, Task task, Exception e) {
    if(e == null) {
      try {
        JSONObject r = new JSONObject(result);
        JSONArray array = r.getJSONArray("data");
        for(int i = 0; i < array.length(); i++)
          busyOrderNumbers.add(array.getInt(i));
        callback.run();
      } catch(JSONException e2) {
        Log.e(TAG, "Exception", e2);
        showError();
      }
    } else showError();
  }

  public Set<Integer> getBusyOrderNumbers() {
    return busyOrderNumbers;
  }

  private void showError() {
    try {
      Toast.makeText(context, context.getText(R.string.loosingHeaders), Toast.LENGTH_LONG).show();
      Dao<Order, Integer> orderDAO = Database.getDAO(Order.class);
      List<Order> orders = orderDAO.queryForAll();
      Dao<OrderCommandHeader, Integer> headerDAO = Database.getDAO(OrderCommandHeader.class);
      List<OrderCommandHeader> headers = headerDAO.queryForAll();
      for(Order o : orders) busyOrderNumbers.add(o.getOrderNumber());
      for(OrderCommandHeader h : headers)
        if(VirtualOrderUtils.virtualize(h).isChanged())
          busyOrderNumbers.add(h.getOrderNumber());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    callback.run();
  }
}

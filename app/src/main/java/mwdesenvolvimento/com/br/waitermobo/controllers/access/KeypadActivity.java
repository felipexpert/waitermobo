package mwdesenvolvimento.com.br.waitermobo.controllers.access;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling.FastSellingActivity;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingServiceImpl;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.OrderNumberControllable;

/**
 * Created by geppetto on 03/08/15.
 */
public class KeypadActivity extends Activity implements OrderNumberControllable {
  private static final String TAG = KeypadActivity.class.getSimpleName();
  private TextView txtOrderNumber;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_simple_tables);
    txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);
    Button[] numbers = {(Button) findViewById(R.id.btn0), (Button) findViewById(R.id.btn1),
        (Button) findViewById(R.id.btn2), (Button) findViewById(R.id.btn3),
        (Button) findViewById(R.id.btn4), (Button) findViewById(R.id.btn5),
        (Button) findViewById(R.id.btn6), (Button) findViewById(R.id.btn7),
        (Button) findViewById(R.id.btn8), (Button) findViewById(R.id.btn9)};
    for (final Button b : numbers)
      b.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          String number = b.getText().toString();
          txtOrderNumber.setText(txtOrderNumber.getText() + number);
        }
      });
    Button btnClean = (Button) findViewById(R.id.btnClean);
    btnClean.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        txtOrderNumber.setText("");
      }
    });
    Button btnConfirm = (Button) findViewById(R.id.btnConfirm);
    new FastSellingServiceImpl(this, getBaseContext(), btnConfirm, true, true).prepare();
  }

  @Override
  protected void onPostResume() {
    super.onPostResume();
    txtOrderNumber.setText("");
  }

  @Override
  public int getOrderNumber() {
    try{
      return Integer.parseInt(txtOrderNumber.getText().toString());
    } catch(NumberFormatException e) {
      return -1;
    }
  }

  @Override
  public void showIdentifiers(int orderNumber) {
    Intent i = new Intent(this, IdentifierActivity.class);
    i.putExtra("orderNumber", orderNumber);
    startActivity(i);
  }

  @Override
  public void showOrder(int headerId) {
    Intent i = new Intent(this, FastSellingActivity.class);
    i.putExtra("headerId", headerId);
    startActivity(i);
  }

  @Override
  public void close() {
    finish();
  }
}
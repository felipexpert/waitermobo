package mwdesenvolvimento.com.br.waitermobo.controllers.access;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling.FastSellingActivity;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.identifier.IdentifierFailureMotive;
import mwdesenvolvimento.com.br.waitermobo.models.identifier.IdentifierHelper;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrderUtils;

/**
 * Created by geppetto on 14/08/15.
 */
public class Add2Frag extends Fragment {
  private Activity activity;
  private int orderNumber;
  private EditText txtIdentifier;
  private Button btnConfirm;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fr_add_order2, container, false);
    txtIdentifier = (EditText) view.findViewById(R.id.txtIdentifier);
    btnConfirm = (Button) view.findViewById(R.id.btnConfirm);
    return view;
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activity = activity;
  }

  public void init(int orderNumber) {
    this.orderNumber = orderNumber;
    btnConfirm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final String ident = txtIdentifier.getText().toString().trim();
        if (!ident.equals("")) {
          IdentifierHelper helper = new IdentifierHelper(ident, new CallbackSuccess() {
            @Override
            public void run() {
              newHeader(ident);
            }
          }, new CallbackFail<IdentifierFailureMotive>() {
            @Override
            public void run(IdentifierFailureMotive identifierFailureMotive) {
              switch (identifierFailureMotive) {
                case CONNECTION:
                  newHeader(ident);
                  break;
                case NEW_IDENTIFIER:
                  Toast.makeText(Add2Frag.this.activity, String.format(getResources().getString(R.string.identifierInUse),
                      ident), Toast.LENGTH_LONG).show();
                  break;
                default:
                  throw new IllegalArgumentException("Unexpected motive");
              }
            }
          });
          helper.perform();
        }
      }
    });
  }

  private void newHeader(String identifier) {
    OrderCommandHeader header = VirtualOrderUtils.createHeader(orderNumber, identifier);
    openHeader(header);
    activity.finish();
  }

  private void openHeader(OrderCommandHeader header) {
    Intent i = new Intent(activity, FastSellingActivity.class);
    i.putExtra("headerId", header.get_id());
    activity.startActivity(i);
  }
}

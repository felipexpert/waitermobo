package mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.access.IdentifierActivity;
import mwdesenvolvimento.com.br.waitermobo.controllers.product.CategoriesActivity;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Listener;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Subject;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.identifier.IdentifierFailureMotive;
import mwdesenvolvimento.com.br.waitermobo.models.identifier.IdentifierHelper;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.FastSellingBridge;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.Item;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.ProductInvalidCodeException;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingService;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingServiceImpl;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingServicePriority;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.OrderNumberControllable;

/**
 * Created by geppetto on 04/08/15.
 */
public class FastSellingActivity extends Activity {
  private static final String TAG = FastSellingActivity.class.getSimpleName();
  public static final String UPDATE_ITEM_SUBJECT = FastSellingActivity.class.getName() + ".updateItem";
  private FastSellingBridge bridge;
  private EditText txtIdentifier;
  private EditText txtObs;
  private TextView lblWaiter;
  private ListView lstItems;
  private TextView lblTotal;
  private EditText txtProductCode;
  private Button btnBack;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_fast_selling);
    try {
      txtIdentifier = (EditText) findViewById(R.id.txtIdentifier);
      txtObs = (EditText) findViewById(R.id.txtObs);
      lblWaiter = (TextView) findViewById(R.id.lblWaiter);
      lstItems = (ListView) findViewById(R.id.lstItems);
      lblTotal = (TextView) findViewById(R.id.lblTotal);
      txtProductCode = (EditText) findViewById(R.id.txtProductCode);
      btnBack = (Button) findViewById(R.id.btnBack);
      Button btnOk = (Button) findViewById(R.id.btnOk);
      Button btnSyncAndPrint = (Button) findViewById(R.id.btnSyncAndPrint);
      Button btnUpdateIdentifier = (Button) findViewById(R.id.btnUpdateIdentifier);
      TextView lblOrderNumber = (TextView) findViewById(R.id.lblOrderNumber);
      Bundle extras = getIntent().getExtras();
      int headerId = extras.getInt("headerId");
      bridge = new FastSellingBridge();
      bridge.setHeader(headerId);
      txtObs.addTextChangedListener(new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
          String oldText = bridge.getObs();
          String newText = txtObs.getText().toString();
          if (!newText.equals(oldText))
            bridge.setObs(newText);
        }
      });
      lstItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          Item item = (Item) parent.getAdapter().getItem(position);
          editItem(item);
        }
      });
      btnOk.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          String productCode = txtProductCode.getText().toString().trim();
          if (productCode.length() > 0) {
            addProduct(productCode);
          } else {
            Subject s = Sys.getInstance().addSubject(UPDATE_ITEM_SUBJECT);
            s.addListener(new Listener() {
              @Override
              public void update(Map<String, Object> data) {
                String command = (String) data.get("fastSelling");
                if ("workDone".equals(command)) {
                  String result = (String) data.get("prodCode");
                  addProduct(result);
                  Sys.getInstance().removeSubject(UPDATE_ITEM_SUBJECT);
                }
              }
            });
            Intent i = new Intent(FastSellingActivity.this, CategoriesActivity.class);
            startActivity(i);
          }
        }
      });
      new FastSellingServiceImpl(new CustomControllable(false), getBaseContext(), btnBack, false, false).prepare();
      new FastSellingServiceImpl(new CustomControllable(true), getBaseContext(), btnSyncAndPrint, false, false).prepare();
      btnUpdateIdentifier.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          final String ident = txtIdentifier.getText().toString();
          if(!"".equals(ident)) {
            new IdentifierHelper(bridge.getHeader(), ident, new CallbackSuccess() {
              @Override
              public void run() {
                Toast.makeText(getBaseContext(), R.string.success, Toast.LENGTH_LONG).show();
                bridge.changeIdentifier(ident);
              }
            }, new CallbackFail<IdentifierFailureMotive>() {
              @Override
              public void run(IdentifierFailureMotive identifierFailureMotive) {
                switch (identifierFailureMotive) {
                  case UPDATING_IDENTIFIER:
                    txtIdentifier.setText(bridge.getIdentifier() != null ? bridge.getIdentifier() : "");
                    Toast.makeText(getBaseContext(), String.format(getResources()
                        .getString(R.string.identifierInUse), ident), Toast.LENGTH_LONG).show();
                    break;
                  case CONNECTION:
                    bridge.changeIdentifier(ident);
                    break;
                  default:
                    throw new IllegalArgumentException("Unexpected motive");
                }
              }
            }).perform();
          }
        }
      });
      if(bridge.getOrderNumber() != -1)
        lblOrderNumber.setText(Integer.toString(bridge.getOrderNumber()));
      else
        ((ViewGroup) lblOrderNumber.getParent()).removeView(lblOrderNumber);
    } catch(SQLException e) {
      Log.e(TAG, "Exception", e);
    }
  }

  private class CustomControllable implements OrderNumberControllable {
    private boolean print;

    public CustomControllable(boolean print) {
      this.print = print;
    }

    @Override
    public int getOrderNumber() {
      return bridge.getOrderNumber();
    }

    @Override
    public void showIdentifiers(final int orderNumber) {
      sync(print, new CallbackSuccess() {
        @Override
        public void run() {
          Intent i = new Intent(FastSellingActivity.this, IdentifierActivity.class);
          i.putExtra("orderNumber", orderNumber);
          startActivity(i);
          Toast.makeText(getBaseContext(), R.string.loadingTable, Toast.LENGTH_LONG).show();
          finish();
        }
      });
    }

    @Override
    public void showOrder(int headerId) {
      sync(print, new CallbackSuccess() {
        @Override
        public void run() {
          finish();
        }
      });
    }

    @Override
    public void close() {
      finish();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    updateInterfce();
  }

  private void updateInterfce() {
    bridge.virtualize();
    txtIdentifier.setText(bridge.getIdentifier() != null ? bridge.getIdentifier() : "");
    txtObs.setText(bridge.getObs());
    lblWaiter.setText(bridge.getWaiter().getName());
    List<Item> items = new ArrayList<>(bridge.getItems());
    lstItems.setAdapter(new ItemAdapter(getBaseContext(), R.id.lstItems, items));
    double total = 0d;
    for(Item i : bridge.getItems())
      total += i.getAmount() * i.getPrice();
    lblTotal.setText(getString(R.string.total) + ": " + Sys.getInstance().currencyFormat(total));
  }
  private void addProduct(String productCode) {
    try {
      bridge.addItem(productCode, 1d);
      updateInterfce();
      txtProductCode.setText("");
      editItem((Item) lstItems.getAdapter().getItem(lstItems.getCount() - 1));
    } catch (ProductInvalidCodeException e) {
      Toast.makeText(getBaseContext(), getString(R.string.invalidProdCode), Toast.LENGTH_SHORT).show();
    }
  }

  private void editItem(Item item) {
    Intent i = new Intent(FastSellingActivity.this, ItemDetailActivity.class);
    i.putExtra("headerId", bridge.getHeader().get_id());
    i.putExtra("displayOrder", item.getDisplayOrder());
    startActivity(i);
  }

  private void sync(boolean print, CallbackSuccess callback) {
    Log.d(TAG, "Going to sync");
    FastSellingService.getInstance().addTask(bridge.getHeader(), print, FastSellingServicePriority.HIGH, callback);
  }

  @Override
  public void onBackPressed() {
    btnBack.performClick();
  }
}

package mwdesenvolvimento.com.br.waitermobo.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by mindware on 26/03/15.
 */
@DatabaseTable(tableName="tbl_category")
public class Category implements Comparable<Category>{
  @DatabaseField(id = true)
  private long _id;
  @DatabaseField
  private String name;
  @ForeignCollectionField
  private Collection<Product> products;

  public Category() {}

  public Category(long _id, String name) {
    this._id = _id;
    this.name = name;
  }

  public long get_id() {
    return _id;
  }

  public void set_id(long _id) {
    this._id = _id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "_id=" + _id + ",name=" + name;
  }

  @Override
  public int compareTo(Category another) {
    return getName().compareToIgnoreCase(another.getName()  );
  }
}

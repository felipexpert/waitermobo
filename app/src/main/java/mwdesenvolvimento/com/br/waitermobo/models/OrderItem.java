package mwdesenvolvimento.com.br.waitermobo.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by geppetto on 24/04/15.
 */
@DatabaseTable(tableName="tbl_order_item")
public class OrderItem implements Comparable<OrderItem> {
    @DatabaseField(id = true)
    private long _id;
    @DatabaseField(foreign = true, foreignAutoCreate = true,
            foreignAutoRefresh=true, maxForeignAutoRefreshLevel=1)
    private Product product;
    @DatabaseField(foreign = true, foreignAutoCreate = true,
            foreignAutoRefresh=true, maxForeignAutoRefreshLevel=1)
    private Order order;
    @DatabaseField
    private double price;
    @DatabaseField
    private double amount;
    @DatabaseField
    private int displayOrder;
    @DatabaseField
    private boolean disabled;

    public OrderItem() {}

    public OrderItem(long _id, Product product, Order order, double price, double amount, int displayOrder) {
        this._id = _id;
        this.product = product;
        this.order = order;
        this.price = price;
        this.amount = amount;
        this.displayOrder = displayOrder;
    }

    public double getAmount()  {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public int compareTo(OrderItem another) {
        return this.getDisplayOrder() - another.getDisplayOrder();
    }

    @Override
    public String toString() {
        return "_id:(" + get_id() + "),productName:(" + getProduct().getName()
                + "),amount(" + getAmount() + "),disabled(" + getDisabled() + ")";
    }
}
package mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Map;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrder;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrderUtils;

/**
 * Created by geppetto on 11/08/15.
 */
public class FastSellingServiceImpl {
  private OrderNumberControllable controllable;
  private Context context;
  private Button button;
  private boolean fromServer;
  private boolean forward;

  public FastSellingServiceImpl(OrderNumberControllable controllable, Context context, Button button, boolean fromServer, boolean forward) {
    this.controllable = controllable;
    this.context = context;
    this.button = button;
    this.fromServer = fromServer;
    this.forward = forward;
  }

  public void prepare() {
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(context, context.getText(R.string.talkingToServer), Toast.LENGTH_SHORT).show();
        if(fromServer)
          FastSellingService.getInstance().verifyOrderNumber(controllable.getOrderNumber(), new CallbackSuccess() {
            @Override
            public void run() {
              goFurther(controllable.getOrderNumber());
            }
          }, new CallbackFail<Synchronizer.SynchronizerListener>() {
            @Override
            public void run(Synchronizer.SynchronizerListener listener) {
              goFurther(controllable.getOrderNumber());
              Toast.makeText(context, context.getText(R.string.loosingHeaders), Toast.LENGTH_SHORT).show();
            }
          });
        else
          goFurther(controllable.getOrderNumber());
      }
    });
  }
  private void goFurther(int orderNumber) {
    Map<OrderCommandHeader, VirtualOrder> map =
        VirtualOrderUtils.virtualOrdersByOrderNumber(orderNumber);

    if (map.size() == 0 && forward) {
        controllable.showOrder(VirtualOrderUtils.createHeader(orderNumber).get_id());
    } else if(map.size() == 1){
      OrderCommandHeader header = map.keySet().iterator().next();
      if(!forward && header.getOrder() == null && !VirtualOrderUtils.virtualize(header).isChanged())
        controllable.close(); // do not consider save empty headers
      else if (header.getIdentifier() == null)
        controllable.showOrder(header.get_id());
      else
        controllable.showIdentifiers(orderNumber);
    } else {
      controllable.showIdentifiers(orderNumber);
    }
  }
}

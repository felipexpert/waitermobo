package mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication;

/**
 * Created by geppetto on 11/08/15.
 */
public interface OrderNumberControllable {
  int getOrderNumber();
  void showIdentifiers(int orderNumber);
  void showOrder(int headerId);
  void close();
}

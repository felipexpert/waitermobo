package mwdesenvolvimento.com.br.waitermobo.controllers.product;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.models.Category;

/**
 * Created by mindware on 27/03/15.
 */
public class CategoryAdapter extends ArrayAdapter<Category> {
  private Context context;
  private List<Category> categories;
  public CategoryAdapter(Context context, int resource, List<Category> categories) {
    super(context, resource, categories);
    this.context = context;
    this.categories = categories;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // 1. Create inflater
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // 2. Get rowView from inflater
    View rowView = inflater.inflate(R.layout.ac_categories_item, parent, false);

    // 3. Get the text view from the rowView
    // 4. Set the text for textView
    String name = categories.get(position).getName();
    name = name.substring(0, 1).toUpperCase() + name.substring(1);
    ((TextView) rowView).setText(name);

    // 5. return rowView
    return rowView;
  }
}

package mwdesenvolvimento.com.br.waitermobo.controllers.waiter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;

import org.json.JSONException;

import java.sql.SQLException;
import java.util.List;

import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import br.com.mwdesenvolvimento.mylibrary.server.JsonServerException;
import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.SystemURL;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.Waiter;

/**
 * Created by mindware on 11/03/15.
 */
public class SelectWaiter extends Activity implements TaskListener {
  private static final String TAG = SelectWaiter.class.getSimpleName();
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_select);
    TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
    txtTitle.setText(getString(R.string.selectWaiter));
    PostRequestTask rt = new PostRequestTask(this);
    rt.execute(SystemURL.GET_WAITERS.getUrl());
  }

  @Override
  public void act(String result, Task task, Exception e) {
    if(e != null) return;
    try {
      Log.d(TAG, result);
      JsonHelper.getRows(result, Waiter.class, true);
      ListView lstWaiters = (ListView) findViewById(R.id.lstItems);
      Dao<Waiter, Integer> dao = Database.getDAO(Waiter.class);
      List<Waiter> waiters = null;
      waiters = dao.queryForAll();
      WaiterAdapter adp = new WaiterAdapter(getBaseContext(), R.layout.ac_waiter_item, waiters);
      lstWaiters.setAdapter(adp);
      lstWaiters.setOnItemClickListener(new ListView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          Waiter waiter = (Waiter) parent.getAdapter().getItem(position);
          Sys.getInstance().setWaiter(waiter);
          finish();
        }
      });
    } catch(SQLException | JSONException | JsonServerException e2) {
      Log.e(TAG, "Exception", e2);
    }

  }
}

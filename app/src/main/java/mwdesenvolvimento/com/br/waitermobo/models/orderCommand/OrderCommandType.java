package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

import java.util.List;

/**
 * Created by geppetto on 26/06/15.
 */
public enum OrderCommandType {
  ADD_ITEM,
  CHANGE_ITEM_AMOUNT,
  REMOVE_ITEM,
  CHANGE_OBS;
}

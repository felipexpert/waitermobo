package mwdesenvolvimento.com.br.waitermobo.models.orderCommand;

/**
 * Created by geppetto on 29/06/15.
 */
public class ProductInvalidCodeException extends Exception {
  public ProductInvalidCodeException() {
    super("This product code could not be found on local database");
  }
}

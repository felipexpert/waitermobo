package mwdesenvolvimento.com.br.waitermobo.models.syncAll;

import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.SystemURL;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackFail;
import mwdesenvolvimento.com.br.waitermobo.models.CallbackSuccess;

/**
 * Created by geppetto on 26/09/15.
 */
public class ConnectionTester implements TaskListener {

  private CallbackSuccess success;
  private CallbackFail<Object> fail;

  public ConnectionTester(CallbackSuccess success, CallbackFail<Object> fail) {
    this.success = success;
    this.fail = fail;
  }

  public static void test(CallbackSuccess success, CallbackFail<Object> fail) {
    new ConnectionTester(success, fail).execute();
  }

  public void execute() {
    PostRequestTask rt = new PostRequestTask(this);
    rt.execute(SystemURL.TEST_CONNECTION.getUrl());
  }

  @Override
  public void act(String result, Task task, Exception e) {
    if(e == null) success.run();
    else          fail.run(null);
  }
}

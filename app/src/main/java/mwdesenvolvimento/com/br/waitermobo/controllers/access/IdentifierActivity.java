package mwdesenvolvimento.com.br.waitermobo.controllers.access;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import br.com.mwdesenvolvimento.mylibrary.Util;
import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling.FastSellingActivity;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.OrderCommandHeader;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrder;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.VirtualOrderUtils;
import mwdesenvolvimento.com.br.waitermobo.models.orderCommand.communication.FastSellingService;

/**
 * Created by geppetto on 03/08/15.
 */
public class IdentifierActivity extends Activity {
  private static final String TAG = IdentifierActivity.class.getSimpleName();
  private int orderNumber;
  private ListView lstHeaders;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    FastSellingService.setWait(true);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_select_identifier);
    lstHeaders = (ListView) findViewById(R.id.lstItems);
    TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
    txtTitle.setText(getString(R.string.identifiers));
    Bundle bundle = getIntent().getExtras();
    orderNumber = bundle.getInt("orderNumber");
    prepare();
  }

  private void prepare() {
    listSetup();
    lstHeaders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        OrderCommandHeader header = (OrderCommandHeader) parent.getAdapter().getItem(position);
        openHeader(header);
        finish();
      }
    });
    Add1Frag frag = (Add1Frag) Util.findFragment(this, R.id.wrapper);
    frag.init(orderNumber);
  }
  private void listSetup() {
    Map<OrderCommandHeader, VirtualOrder> vos = VirtualOrderUtils.virtualOrdersByOrderNumber(orderNumber);
    ArrayList<OrderCommandHeader> vos2 = new ArrayList<>(vos.keySet());
    Collections.sort(vos2, new Comparator<OrderCommandHeader>() {
      @Override
      public int compare(OrderCommandHeader lhs, OrderCommandHeader rhs) {
        String lhs1 = lhs.getIdentifier() != null ? lhs.getIdentifier() : "";
        String rhs1 = rhs.getIdentifier() != null ? rhs.getIdentifier() : "";
        return lhs1.compareToIgnoreCase(rhs1);
      }
    });
    JsonHelper.populateList(this, vos2, R.id.lstItems,
            R.layout.ac_order, IdentifierAdapter.class);
  }

  private void openHeader(OrderCommandHeader header) {
    Intent i = new Intent(IdentifierActivity.this, FastSellingActivity.class);
    i.putExtra("headerId", header.get_id());
    startActivity(i);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    FastSellingService.setWait(false);
  }
}

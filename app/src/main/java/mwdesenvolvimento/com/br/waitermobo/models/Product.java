package mwdesenvolvimento.com.br.waitermobo.models;

import android.content.res.Resources;
import android.util.Log;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.NumberFormat;
import java.util.Locale;

import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;

/**
 * Created by mindware on 23/03/15.
 */
@DatabaseTable(tableName="tbl_product")
public class  Product implements Comparable<Product>{
  @DatabaseField(id = true)
  private long _id;
  @DatabaseField
  private String code;
  @DatabaseField(foreign = true, foreignAutoCreate = true,
      foreignAutoRefresh=true, maxForeignAutoRefreshLevel=1)
  private Category category;
  @DatabaseField
  private String name;
  @DatabaseField
  private double price;

  public Product() {}

  public Product(long _id, String code, Category category, String name, double price) {
    this._id = _id;
    this.code = code;
    this.category = category;
    this.name = name;
    this.price = price;
  }

  public long get_id() {
    return _id;
  }

  public void set_id(long _id) {
    this._id = _id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  /*public void setCategory(Category category) {
    this.category = category;
  }*/

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getCategoryName() {
    if(category != null && category.getName() != null)
      return category.getName();
    return null;
  }

  @Override
  public String toString() {
    return "_id=" + get_id() + ",name=" + getName() + ",price=" + getPrice() + ",category=" + getCategoryName();
  }

  @Override
  public int compareTo(Product another) {
    return getName().compareToIgnoreCase(another.getName());
  }
}

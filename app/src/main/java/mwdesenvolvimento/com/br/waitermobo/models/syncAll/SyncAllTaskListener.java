package mwdesenvolvimento.com.br.waitermobo.models.syncAll;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import br.com.mwdesenvolvimento.mylibrary.server.JsonServerException;
import br.com.mwdesenvolvimento.mylibrary.server.PostRequestTask;
import br.com.mwdesenvolvimento.mylibrary.server.Task;
import br.com.mwdesenvolvimento.mylibrary.server.TaskListener;
import mwdesenvolvimento.com.br.waitermobo.SystemURL;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.Order;
import mwdesenvolvimento.com.br.waitermobo.models.Product;
import mwdesenvolvimento.com.br.waitermobo.models.Waiter;

/**
 * Created by geppetto on 04/05/15.
 */
public class SyncAllTaskListener implements TaskListener {
    private static final String TAG = SyncAllTaskListener.class.getSimpleName();
    private TaskListener tl;
    private int tasksDone;

    public SyncAllTaskListener(TaskListener tl) {
        this.tl = tl;
    }

    public void execute() {
        PostRequestTask rt = new PostRequestTask(this, Task.RESET_ENTITIES);
        rt.execute(SystemURL.TEST_CONNECTION.getUrl());
    }

    @Override
    public void act(String result, Task task, Exception e) {
        try {
            if(task != Task.RESET_ENTITIES && task != Task.GET_WAITERS && task != Task.GET_ORDERS && task != Task.GET_PRODS && task != Task.TABLES_AMOUNT)
                throw new IllegalArgumentException(SyncAllTaskListener.class.getSimpleName()
                    + " can't manage " + task + " task");
            switch (task) {
                case RESET_ENTITIES:
                    if(e == null) {
                        try {
                            JSONObject json = new JSONObject(result);
                            String status = json.getString("_status");
                            if (status.equals("success")) {
                                Database.getInstance().resetAllEntities();
                                PostRequestTask rt = new PostRequestTask(this, Task.GET_PRODS);
                                rt = new PostRequestTask(this, Task.GET_WAITERS);
                                rt.execute(SystemURL.GET_WAITERS.getUrl());
                                rt = new PostRequestTask(this, Task.GET_PRODS);
                                rt.execute(SystemURL.PRODUCTS.getUrl());
                                rt = new PostRequestTask(this, Task.GET_ORDERS);
                                rt.execute(SystemURL.GET_ORDERS.getUrl());
                                rt = new PostRequestTask(this, Task.TABLES_AMOUNT);
                                rt.execute(SystemURL.TABLES_AMOUNT.getUrl());
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                    break;
                case GET_WAITERS:
                    JsonHelper.getRows(result, Waiter.class, true);
                    break;
                case GET_PRODS:
                    JsonHelper.getRows(result, Product.class, true);
                    break;
                case GET_ORDERS:
                    JsonHelper.getRows(result, Order.class, true);
                    break;
                case TABLES_AMOUNT:
                    JSONObject r = new JSONObject(result);
                    int tablesAmount = r.getInt("tables");
                    Sys.getInstance().setTotalOrderNumbers(tablesAmount);
                    break;
            }
            tasksDone++;
            if(tasksDone == 5)
                tl.act(result, task, e);
        } catch(JsonServerException | JSONException e2) {
            Log.e(TAG, "Exception", e2);
        }
    }
}

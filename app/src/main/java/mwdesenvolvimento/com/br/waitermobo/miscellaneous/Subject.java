package mwdesenvolvimento.com.br.waitermobo.miscellaneous;

/**
 * Created by geppetto on 04/05/15.
 */
public interface Subject {
  Subject NULL = new Subject() {
    public void addListener(Listener l) {}
    public boolean removeListener(Listener l) { return false; }
    public void setData(String key, Object data) {}
    public void notifyAllListeners() {}
  };

  void addListener(Listener l);
  boolean removeListener(Listener l);
  void setData(String key, Object data);
  void notifyAllListeners();
}

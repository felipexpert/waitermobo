package mwdesenvolvimento.com.br.waitermobo.controllers.product;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.mwdesenvolvimento.mylibrary.server.JsonHelper;
import mwdesenvolvimento.com.br.waitermobo.R;
import mwdesenvolvimento.com.br.waitermobo.controllers.fastSelling.FastSellingActivity;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Database;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Listener;
import mwdesenvolvimento.com.br.waitermobo.miscellaneous.Sys;
import mwdesenvolvimento.com.br.waitermobo.models.Product;

/**
 * Created by mindware on 27/03/15.
 */
public class ProdsActivity extends Activity implements Listener {
  private static final String TAG = ProdsActivity.class.getSimpleName();
  private ArrayAdapter<Product> adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_products);
    TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
    final EditText txtFilter = (EditText) findViewById(R.id.txtFilter);
    txtFilter.addTextChangedListener(new TextWatcher() {
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      public void onTextChanged(CharSequence s, int start, int before, int count) {}
      public void afterTextChanged(Editable s) {
        if(adapter != null) {
          adapter.getFilter().filter(txtFilter.getText());
        }
      }
    });
    Bundle extras = getIntent().getExtras();
    boolean hasFilter = extras.getBoolean("hasFilter");
    try {
      List<Product> prods = null;
      Dao<Product, Integer> dao = Database.getDAO(Product.class);
      if(hasFilter) {
        long idCat = extras.getLong("idCat");
        PreparedQuery<Product> pq =
            dao.queryBuilder().where().eq("category_id", idCat).prepare();
        prods = dao.query(pq);
        txtTitle.setText(prods.get(0).getCategoryName());
      } else {
        prods = dao.queryForAll();
        txtTitle.setText(R.string.prods);
      }
      JsonHelper.sortList(prods, Product.class);
      JsonHelper.populateList(this, prods, R.id.lstItems, R.layout.ac_prods_item,
          ProdsAdapter.class);
      ListView lslItems = (ListView) findViewById(R.id.lstItems);
      adapter = (ArrayAdapter<Product>) lslItems.getAdapter();
    } catch(SQLException e) {
      Log.e(TAG, "Exception", e);
    }
    Sys.getInstance().listenToSubject(FastSellingActivity.UPDATE_ITEM_SUBJECT, this);
    ListView lstItems = (ListView) findViewById(R.id.lstItems);
    lstItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemClick");
        Product p = (Product) parent.getAdapter().getItem(position);
        Map<String, Object> data = new HashMap<>();
        data.put("fastSelling", "workDone");
        data.put("prodCode", p.getCode());
        data.put("category", "close");
        data.put("product", "close");

        Sys.getInstance().setDataAndNotify(FastSellingActivity.UPDATE_ITEM_SUBJECT, data);
      }
    });
  }

  @Override
  public void update(Map<String, Object> data) {
    String command = (String) data.get("product");
    if("close".equals(command)) finish();
  }
}
package mwdesenvolvimento.com.br.waitermobo.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by geppetto on 18/08/15.
 */
@DatabaseTable(tableName="tbl_msn")
public class Msn {
  @DatabaseField(generatedId = true)
  private int _id;
  @DatabaseField
  private String message;

  public Msn() {}

  public Msn(int _id, String message) {
    this._id = _id;
    this.message = message;
  }

  public int get_id() {
    return _id;
  }

  public void set_id(int _id) {
    this._id = _id;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}

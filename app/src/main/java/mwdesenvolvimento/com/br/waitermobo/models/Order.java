package mwdesenvolvimento.com.br.waitermobo.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by mindware on 27/03/15.
 */
@DatabaseTable(tableName="tbl_order")
public class Order {
  @DatabaseField(id = true)
  private long _id;
  @DatabaseField
  private int orderNumber;
  @DatabaseField
  private String identifier;
  //@DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
  //private Date closingDate;
  @ForeignCollectionField
  private Collection<OrderItem> orderItemCollection = new ArrayList<>();
  @DatabaseField
  private String obs;
  @DatabaseField(foreign = true, foreignAutoCreate = true,
      foreignAutoRefresh=true, maxForeignAutoRefreshLevel=1)
  private Waiter waiter;
  public Order() {}

  public Order(long _id, int orderNumber, String identifier,
               Collection<OrderItem> orderItemCollection, String obs, Waiter waiter) {
    this._id = _id;
    this.orderNumber = orderNumber;
    this.identifier = identifier;
    this.orderItemCollection = orderItemCollection;
    this.obs = obs;
    this.waiter = waiter;
  }

  public long get_id() {
    return _id;
  }

  public void set_id(long _id) {
    this._id = _id;
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(int orderNumber) {
    this.orderNumber = orderNumber;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public Collection<OrderItem> getOrderItemCollection() {
    return orderItemCollection;
  }

  public void setOrderItemCollection(Collection<OrderItem> orderItemCollection) {
    this.orderItemCollection = orderItemCollection;
  }

  public String getObs() {
    return obs;
  }

  public void setObs(String obs) {
    this.obs = obs;
  }

  public Waiter getWaiter() {
    return waiter;
  }

  public void setWaiter(Waiter waiter) {
    this.waiter = waiter;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for(OrderItem oi : getOrderItemCollection())
      sb.append("id:(" + oi.get_id() + "),orderId:(" + oi.getOrder().get_id() + "),productName:(" + oi.getProduct().getName()
          + "),amount:(" + oi.getAmount() + "),price:(" + oi.getPrice() + ")");
    sb.append("]");
    return "_id:(" + get_id() + "),,orderNumber:(" + getOrderNumber() + "),identifier:("
        + identifier + "),orderItemCollection:(" + sb + "),obs:(" + getObs() + ")";
  }

  public double total() {
    double total = 0d;
    for(OrderItem oi : orderItemCollection) if(!oi.getDisabled())
      total += oi.getAmount() * oi.getPrice();
    return total;
  }
}
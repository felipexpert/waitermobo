package mwdesenvolvimento.com.br.waitermobo.controllers.access;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import br.com.mwdesenvolvimento.mylibrary.Util;
import mwdesenvolvimento.com.br.waitermobo.R;

/**
 * Created by geppetto on 14/08/15.
 */
public class Add1Frag extends Fragment {
  private Button btnAdd;
  private Activity activity;
  public void init(final int orderNumber) {
    btnAdd.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Add2Frag frag = new Add2Frag();
        Util.putFragment(activity, R.id.wrapper, frag);
        frag.init(orderNumber);
      }
    });
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fr_add_order1, container, false);
    btnAdd = (Button) view.findViewById(R.id.btnAdd);
    return view;
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activity = activity;
  }
}

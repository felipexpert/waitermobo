package mwdesenvolvimento.com.br.waitermobo.controllers;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import mwdesenvolvimento.com.br.waitermobo.R;

/**
 * Created by geppetto on 14/08/15.
 */
public class HeaderFrag extends Fragment {
  private Activity activity;
  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fr_header, container, false);
    Button btnBack = (Button) view.findViewById(R.id.btnBack);
    btnBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        activity.onBackPressed();
      }
    });
    return view;
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activity = activity;
  }
}

package mwdesenvolvimento.com.br.waitermobo.miscellaneous;

import java.util.Map;

/**
 * Created by geppetto on 04/05/15.
 */
public interface Listener {
  void update(Map<String, Object> data);
}
